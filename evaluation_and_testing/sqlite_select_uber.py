# This script executes select query for [5000, 10000, 50000, 100000, 500000, 1000000] times
# and records the time for each.

# Broke the sqlite select script to run each command in a separate process
# as the sqlite engine caches data after first execution.

# Command to execute:
# python sqlite_select_uber.py

import os

os.system("python sqlite_select.py 10")
os.system("python sqlite_select.py 50")
os.system("python sqlite_select.py 100")
os.system("python sqlite_select.py 500")
os.system("python sqlite_select.py 1000")
os.system("python sqlite_select.py 1500")
