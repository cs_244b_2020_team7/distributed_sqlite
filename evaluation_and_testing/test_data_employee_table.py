# The below code creates an Employee table in dsqlite and enters sample data into it for testing

# Command to run the script
# python test_data_employee_table.py [num_of_records]

# The below queries are executed to create an Employee table and populate it with Employee records
# CREATE TABLE Employee (ID numeric, FIRST_NAME text, LAST_NAME text, STATE text, DATE_OF_BIRTH text);
# INSERT INTO Employee(ID, FIRST_NAME, LAST_NAME, STATE, DATE_OF_BIRTH) VALUES (1, Abe, Lincoln, "KY", "02/12/1809")
import sys

import requests
import json

from data_builder import DataBuilder

TOTAL_RECORD_COUNT = 50

SQL_URL = 'http://localhost:8080/sql'
HEADERS = {"Content-Type": "application/json"}
PAYLOAD = {
    "id": "",
    "dbName": "dsqlite",
    "groupId": "dsqlite",
    "sqlCommand": ""
}

if len(sys.argv) >= 2:
    TOTAL_RECORD_COUNT = int(sys.argv[1])

# Prepare data to be inserted
data = DataBuilder()
employee_data = data.get_employee_data(TOTAL_RECORD_COUNT)
queries = data.get_queries()

# Delete previous data if exists and create Employee table
PAYLOAD["id"] = 1
PAYLOAD["sqlCommand"] = queries['DROP_EMPLOYEE_TABLE']
requests.post(SQL_URL, data=json.dumps(PAYLOAD), headers=HEADERS)

PAYLOAD["id"] = 2
PAYLOAD["sqlCommand"] = queries['CREATE_EMPLOYEE_TABLE']
requests.post(SQL_URL, data=json.dumps(PAYLOAD), headers=HEADERS)

# Insert randomly generated data into Employee table
print("Inserting records into table")
for index, employee in enumerate(employee_data):
    query = queries['INSERT_EMPLOYEE_RECORD'].format(employee['id'], employee['first'], employee['last'],
                                                     employee['state'], employee['dob'])
    PAYLOAD["id"] = index
    PAYLOAD["sqlCommand"] = query
    requests.post(SQL_URL, data=json.dumps(PAYLOAD), headers=HEADERS)

print(str(TOTAL_RECORD_COUNT) + " records inserted.")
