# Code to parse data from json file and dynamically create Employee records

import json
import random
import time


class DataBuilder:

    DATA_FILE = 'data.json'
    data = {}

    def __init__(self):
        with open(self.DATA_FILE, 'r') as datafile:
            # Load data into json file
            self.data = json.load(datafile)

    def get_employee_data(self, record_count):
        employee_data = []
        for x in range(record_count):
            record = {
                'id': x + 1,
                'first': self.data['EMPLOYEE_FIRST_NAMES'][random.randint(0, len(self.data['EMPLOYEE_FIRST_NAMES']) - 1)],
                'last': self.data['EMPLOYEE_LAST_NAMES'][random.randint(0, len(self.data['EMPLOYEE_LAST_NAMES']) - 1)],
                'state': self.data['PLACES'][random.randint(0, len(self.data['PLACES']) - 1)],
                'dob': self.__random_date("%m/%d/%Y")
            }
            employee_data.append(record)
        return employee_data

    def __random_date(self, form):
        stime = time.mktime(time.strptime("1/1/1980", form))
        etime = time.mktime(time.strptime("12/31/2010", form))
        ptime = stime + random.random() * (etime - stime)
        return time.strftime(form, time.localtime(ptime))

    def get_queries(self):
        return self.data['QUERIES']
