# -*- coding: utf-8 -*-

import requests
import json

#import matplotlib.pyplot as plt

url     = 'http://localhost:8080/sql'
payload = { "id":"234", "dbName":"newdb","groupId":"dsqlite", "sqlCommand":"CREATE TABLE bar (id INTEGER NOT NULL PRIMARY KEY,name TEXT);" }
headers = {"Content-Type" :"application/json"}
time = []
res = requests.post(url, data=payload, headers=headers)
time.append(res.elapsed.total_seconds())

for i in range(0, 2000):
    payload = { "id":"234", "dbName":"newdb","groupId":"dsqlite", "sqlCommand":"Insert Into bar(name) VALUES(“Fiona" + str(i) + ");"}

    res = requests.post(url, data=json.dumps(payload), headers=headers)
    print(res)
    time.append(time[-1] + res.elapsed.total_seconds())
print(time)

payload = { "id":"234", "dbName":"newdb","groupId":"dsqlite", "sqlCommand":"SELECT * FROM * FROM foo WHERE name like \"Fiona\";" }
headers = {"Content-Type" :"application/json"}
time = []
res = requests.get(url, data=payload, headers=headers)
time.append(res.elapsed.total_seconds())

for i in range(0, 2000):
    payload = { "id":"234", "dbName":"newdb","groupId":"dsqlite", "sqlCommand":"select * from bar where id like \"fiona" + str(i) + "\"" }
    res = requests.post(url, data=json.dumps(payload), headers=headers)
    print(res)
    time.append(time[-1] + res.elapsed.total_seconds())
print(time)
#plt.plot(time)
