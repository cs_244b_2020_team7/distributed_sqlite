# The below code runs scale tests for INSERT & SELECT query on Distributed SQLite

# Command to run the script
# python dsqlite.py

# The below queries are executed to create an Employee table and populate it with Employee records
# CREATE TABLE Employee (ID numeric, FIRST_NAME text, LAST_NAME text, STATE text, DATE_OF_BIRTH text);
# INSERT INTO Employee(ID, FIRST_NAME, LAST_NAME, STATE, DATE_OF_BIRTH) VALUES (1, Abe, Lincoln, "KY", "02/12/1809")

# Sample data previously generated:

# Time taken to INSERT records into table
# For 10 records: 0.7724881172180176 seconds.
# For 50 records: 4.004645109176636 seconds.
# For 100 records: 7.75806999206543 seconds.
# For 500 records: 33.710302114486694 seconds.
# For 1000 records: 64.971351146698 seconds.
# For 1500 records: 95.11530804634094 seconds.
# For 2000 records: 123.98383712768555 seconds.

# Time taken to SELECT records from table
# For 10 records: 0.06756830215454102 seconds.
# For 50 records: 0.10909509658813477 seconds.
# For 100 records: 0.07421517372131348 seconds.
# For 500 records: 0.26419711112976074 seconds.
# For 1000 records: 0.3765730857849121 seconds.
# For 1500 records: 0.6127550601959229 seconds.
# For 2000 records: 0.4349651336669922 seconds.

import requests
import json
import time

from data_builder import DataBuilder

SQL_URL = 'http://localhost:8080/sql'
HEADERS = {"Content-Type": "application/json"}
PAYLOAD = {
    "id": "",
    "dbName": "dsqlite",
    "groupId": "dsqlite",
    "sqlCommand": ""
}

TOTAL_RECORD_COUNT = 5000
RECORDS = [10, 50, 100, 500, 1000, 1500, 2000, 3000, 4000, 5000]
PERFORM_INSERT = False
PERFORM_SELECT = True

# Prepare data to be inserted
data = DataBuilder()
employee_data = data.get_employee_data(TOTAL_RECORD_COUNT)
queries = data.get_queries()


# Delete previous data if exists and create Employee table
PAYLOAD["id"] = 1
PAYLOAD["sqlCommand"] = queries['DROP_EMPLOYEE_TABLE']
requests.post(SQL_URL, data=json.dumps(PAYLOAD), headers=HEADERS)

PAYLOAD["id"] = 2
PAYLOAD["sqlCommand"] = queries['CREATE_EMPLOYEE_TABLE']
requests.post(SQL_URL, data=json.dumps(PAYLOAD), headers=HEADERS)

if PERFORM_INSERT:
    # Insert randomly generated data into Employee table
    print("Time taken to INSERT records into table")
    i = 0
    start = time.time()
    for index, employee in enumerate(employee_data):
        query = queries['INSERT_EMPLOYEE_RECORD'].format(employee['id'], employee['first'], employee['last'], employee['state'], employee['dob'])
        PAYLOAD["id"] = index
        PAYLOAD["sqlCommand"] = query
        requests.post(SQL_URL, data=json.dumps(PAYLOAD), headers=HEADERS)

        if index == (RECORDS[i]-1):
            done = time.time()
            elapsed = done - start
            print("For " + str(RECORDS[i]) + " records: " + str(elapsed) + " seconds.")
            i += 1

if PERFORM_SELECT:
    # Read given number of records from Employee table and note the time
    print("\nTime taken to SELECT records from table")
    for records in RECORDS:
        query = queries['SELECT_FROM_EMPLOYEE_TABLE'].format(records)
        PAYLOAD["id"] = records
        PAYLOAD["sqlCommand"] = query
        time.sleep(5)
        start = time.time()
        requests.post(SQL_URL, data=json.dumps(PAYLOAD), headers=HEADERS)
        done = time.time()
        elapsed = done - start
        print("For " + str(records) + " records: " + str(elapsed) + " seconds.")
