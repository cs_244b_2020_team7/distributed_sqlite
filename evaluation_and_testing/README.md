# Evaluation & Testing

## Testing
To create a table and enter some sample data into your instance of Distributed SQLite, use the below script.
> python test_data_employee_table.py [num_of_records]

## Evaluation

We evaluated our project against rqlite (an open sourced distributed sqlite database) and a reqular sqlite instance.

## DSQLite & rqlite

rqlite is an open sourced distributed sqlite database (https://github.com/rqlite/rqlite). To evaluate the two we used the below scripts to generate the comparison data.

> rqlite_benchmark.py <br>
> dsqlite_benchmark.py

![DSQLite & rqlite UPDATE comparison](DSQLite_rqlite_update_times.png)

![DSQLite & rqlite READ comparison](DSQLite_rqlite_read_times.png)

## DSQLite & SQLite

To compare DSQlite and SQLite we used the below scripts to query the databases multiple times using the same queries for both the systems.

Queries used:
> CREATE TABLE Employee (ID numeric, FIRST_NAME text, LAST_NAME text, STATE text, DATE_OF_BIRTH text) <br><br>
> INSERT INTO Employee (ID, FIRST_NAME, LAST_NAME, STATE, DATE_OF_BIRTH) VALUES ({0},'{1}','{2}','{3}','{4}') <br><br>
> SELECT * FROM Employee LIMIT {0}

To generate UPDATE and READ stats for sqlite
> python sqlite_insert.py <br>
> python sqlite_select_uber.py

To generate UPDATE and READ stats for dsqlite
> python dsqlite.py

DSQLite & SQLite UPDATE comparison
![DSQLite & SQLite UPDATE comparison](DSQLite_SQLite_update_times.png)


DSQLite & SQLite READ comparison
![DSQLite & SQLite READ comparison](DSQLite_SQLite_read_times.png)


## DSQLite Strong READ and Weak READ

We have implemented the ability to make read requests with Strong and Weak consistency. Below is a comparison of the times for read requests made using Strong and Weak consistency. In case of Weak reads the request does not go through the regular Raft log replication process and hence is much faster in response.

![DSQLite Strong READ and Weak READ](DSQLite_Strong_Weak_Read.png)

