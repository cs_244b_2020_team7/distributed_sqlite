# The code below runs scale tests on INSERT query on SQLite

# Command to run the script
# python sqlite_insert.py

# The below queries are executed to create an Employee table and populate it with Employee records
# CREATE TABLE Employee (ID numeric, FIRST_NAME text, LAST_NAME text, STATE text, DATE_OF_BIRTH text);
# INSERT INTO Employee(ID, FIRST_NAME, LAST_NAME, STATE, DATE_OF_BIRTH) VALUES (1, Abe, Lincoln, "KY", "02/12/1809")


import sqlite3
import time

from data_builder import DataBuilder

TOTAL_RECORD_COUNT = 2000
RECORDS = [10, 50, 100, 500, 1000, 1500, 2000]


# Prepare data to be inserted
data = DataBuilder()
employee_data = data.get_employee_data(TOTAL_RECORD_COUNT)
queries = data.get_queries()

conn = sqlite3.connect('test.db',)

# Delete previous data if exists and create Employee table
conn.execute(queries['DROP_EMPLOYEE_TABLE'])
conn.execute(queries['CREATE_EMPLOYEE_TABLE'])

# Insert randomly generated data into Employee table
print("Time taken to INSERT records into table")
i = 0
start = time.time()
for index, employee in enumerate(employee_data):
    query = queries['INSERT_EMPLOYEE_RECORD'].format(employee['id'], employee['first'], employee['last'], employee['state'], employee['dob'])
    conn.execute(query)

    if index == (RECORDS[i]-1):
        done = time.time()
        elapsed = done - start
        print("For " + str(RECORDS[i]) + " records: " + str(elapsed) + " seconds.")
        i += 1

conn.close()
