# -*- coding: utf-8 -*-

import requests
import json

import urllib2, urllib
#import matplotlib.pyplot as plt

url     = 'http://localhost:4001/db/execute?pretty&timings'
payload = ["DELETE from foo"]
headers = {"Content-Type" :"application/json"}
time = []
res = requests.post(url, data=json.dumps(payload), headers=headers)
time.append(res.elapsed.total_seconds())

for i in range(0, 1500):
    payload = ["INSERT INTO foo(name) VALUES(\"Fiona" + str(i) + "\");"]
    print(payload)
    res = requests.post(url, data=json.dumps(payload), headers=headers)
    #print(res)
    time.append(time[-1] + res.elapsed.total_seconds())

print(time)
read_time = []
read_url = 'http://localhost:4001/db/query?pretty&timings&level=strong'
for i in range(0, 2000):
    payload = {'q': "SELECT * FROM foo WHERE name like \"Fiona" + str(i) + "%\""}
    print(payload)
    res = requests.get(read_url, params=payload)
    #print(res.json())
    if (len(read_time) > 0):
        read_time.append(read_time[-1] + res.elapsed.total_seconds())
    else:
        read_time.append(res.elapsed.total_seconds())
print(read_time)
#plt.plot(time)
