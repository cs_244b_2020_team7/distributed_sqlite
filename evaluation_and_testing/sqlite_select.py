# The code below runs scale tests on SELECT query on SQLite

# Command to run the script
# python sqlite_insert.py num_of_records
# num_of_records (optional) - number of records you want to read from DB. Can be values like 5000, 10000 etc.
# Eg: python sqlite_insert.py 500

import sqlite3
import time
import sys

from data_builder import DataBuilder

RECORDS = 10000000  # Default records value
SELECT_PRINT_COL_WIDTH = 15

if len(sys.argv) >= 2:
    RECORDS = sys.argv[1]

data = DataBuilder()
queries = data.get_queries()

print("Time taken to SELECT " + str(RECORDS) + " records from table")

conn = sqlite3.connect('test.db', )
start = time.time()
employees = conn.execute(queries['SELECT_FROM_EMPLOYEE_TABLE'].format(RECORDS))
done = time.time()
elapsed = done - start
print("For " + str(RECORDS) + " records: " + str(elapsed) + " seconds.")
conn.close()

# Print content of SELECT query
# employees = conn.execute("SELECT * FROM Employee")
# col_width = SELECT_PRINT_COL_WIDTH
# columns = ["ID", "FIRST NAME", "LAST NAME", "STATE", "DATE OF BIRTH"]
# print("".join(word.ljust(col_width) for word in columns))
# for record in employees:
#     print("".join(str(word).ljust(col_width) for word in record))



