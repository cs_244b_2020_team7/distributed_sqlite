# Distributed_Sqlite

A distributed relational database built upon SQLite and Raft algorithm.

Project Members:
Ethan Qiu
Esther (CUIPING) Fang
Purva Kamat
Arun Rajan


To run a simple example:

- Start up 3 raft servers on different address:port:   
 
	    mvn exec:java -Dexec.mainClass="edu.stanford.cs244b.sofajraft.service.SqliteRaftServer" -Dexec.args="/tmp/server1 dsqlite 127.0.0.1:8443"
	    mvn exec:java -Dexec.mainClass="edu.stanford.cs244b.sofajraft.service.SqliteRaftServer" -Dexec.args="/tmp/server2 dsqlite 127.0.0.1:8444"
	    mvn exec:java -Dexec.mainClass="edu.stanford.cs244b.sofajraft.service.SqliteRaftServer" -Dexec.args="/tmp/server3 dsqlite 127.0.0.1:8445"
- Issue client request:  
    
        mvn exec:java -Dexec.mainClass="edu.stanford.cs244b.sofajraft.client.DistributedSqliteClient" -Dexec.args="dsqlite"
- For distributed raft request, we need to start Coordinator and other group of raft servers
        
        mvn exec:java -Dexec.mainClass="edu.stanford.cs244b.twopc.DefaultCoordinator" 
        mvn exec:java -Dexec.mainClass="edu.stanford.cs244b.sofajraft.service.SqliteRaftServer" -Dexec.args="/tmp/server4 group2 127.0.0.1:8501"
        mvn exec:java -Dexec.mainClass="edu.stanford.cs244b.sofajraft.service.SqliteRaftServer" -Dexec.args="/tmp/server5 group2 127.0.0.1:8502"
        mvn exec:java -Dexec.mainClass="edu.stanford.cs244b.sofajraft.service.SqliteRaftServer" -Dexec.args="/tmp/server6 group2 127.0.0.1:8503"



    

Docker

> python deploy_servers.py --groupId [RAFT_GROUP_ID]
Note - Using Python 3

This script does the following:

1. Deploys Raft Node Servers - wait for them assign IP - Modifies the configuartion.json file to write newly acquired IPs.
2. Compiles the code using Maven Assembly Plugin and run some container commands to copy the fat jar file.
3. Creates REPL client container - Since it is interactive in nature we can start it in a different terminal
> docker run -i containerID

4. Deploys 2 Phase Commit Coordinator

5. Recompiles the code with SpringBoot Plugin
6. Deploys a webserver for REST requests and maps port 8080 to the host port 8080


Useful Commands
1. docker ps                            (to see running containers)
2. docker stop $(docker ps -a -q)       (to stop all running containers)
3. docker rm $(docker ps -a -q)         (to remove all containers)
4. docker images                        (to see all the images)
5. docker rmi $(docker images -q)       (to remove all the images)
