# DsqliteAngularClient

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.5.

## How to start the UI client?

The UI client can be started by the below two methods:

## 1. To run locally

To run the app locally, run the below command in the path containing package.json which is in the top level directory of the project.

> npm start

## 2. To run using docker

To run the app using docker containers, use the below commands to create the docker image and run the container.

Create the docker image using Dockerfile
> docker build -f dsqlite-angular-client.Dockerfile -t dsqlite-angular-client .

Start the container
> docker run -p 4200:4200 --name dsqlite-angular-client -it dsqlite-angular-client
 
 The UI can then be accessed at http://localhost:4200/

![UI Client Main page](UI_Main.png)


![UI Client Confugure cluster page](UI_Configure.png)
