FROM node:12-alpine

# Create app directory
WORKDIR /home/dsqlite

# Copy source code
COPY . /home/dsqlite

# Install packages required for application
RUN npm install

# Install angular-cli packages
RUN npm install -g @angular/cli

# Expose port where app is running
EXPOSE 4200/tcp

# Command to start the app when container starts
CMD ng serve --host 0.0.0.0
