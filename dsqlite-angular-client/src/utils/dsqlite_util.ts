import {Constants} from './constants';
import {Host} from '../models/Host';
import {environment} from '../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Configuration} from './configuration';
import { UUID } from 'angular2-uuid';

@Injectable()
export class DsqliteUtil {

  constructor(private constants: Constants,
              private http: HttpClient,
              private config: Configuration) {
  }

  fetchCurrentMembers(errorCallback) {
    this.config.hosts = [];
    let params = new HttpParams();
    params = params.append('groupId', this.config.clusterName);
    this.http.get(environment.server + this.constants.LIST_MEMBERS_API, {params}).subscribe((response: any) => {
        if (response) {
          // parse leader info
          let leader = null;
          if ('leaderAddress' in response) {
            const leaderAddress = response.leaderAddress.split(':');
            leader = new Host(leaderAddress[0], leaderAddress[1], true);
            this.config.leader = leader;
          }

          // parse member info
          if ('peerInfoList' in response) {
            const members = response.peerInfoList;
            for (const member of members) {
              const host = new Host(member.ip, member.port, (leader && (leader.ip === member.ip) && (leader.port == member.port)));
              this.config.hosts.push(host);
            }
          }
        }
      },
      (error) => {
        if (errorCallback) {
          errorCallback(error);
        }
      });
  }

  fetchTablesInDatabase() {
    this.config.tables = [];
    const body = {
      id: UUID.UUID(),
      dbName: this.config.clusterName,
      groupId: this.config.clusterName,
      sqlCommand: this.constants.SQL_QUERY.LIST_TABLES
    };
    this.http.post(environment.server + this.constants.EXECUTE_SQL_API, body).subscribe((response: any) => {
      if ('result' in response && 'commandType' in response && response.commandType === 'READ') {
        const tableResponse = JSON.parse(response.result);
        if (tableResponse && tableResponse.length) {
          for (const tr of tableResponse) {
            this.config.tables.push(tr.name);
          }
        }
      }
    });
  }

  executeQuery(sqlCommand) {
    const body = {
      id: UUID.UUID(),
      dbName: this.config.clusterName,
      groupId: this.config.clusterName,
      sqlCommand
    };
    return this.http.post(environment.server + this.constants.EXECUTE_SQL_API, body);
  }

  changeMembers(member: Host, action) {
    const body = {
      ip : member.ip,
      port : '' + member.port,
      groupId : this.config.clusterName,
      action
    };
    return this.http.post(environment.server + this.constants.MEMBERS_MODIFY_API, body);
  }
}
