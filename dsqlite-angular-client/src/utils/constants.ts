import {Injectable} from '@angular/core';

@Injectable()
export class Constants {
  LIST_MEMBERS_API = '/members';
  MEMBERS_MODIFY_API = '/members/modify';
  EXECUTE_SQL_API = '/sql';

  MODIFY_MEMBER_TYPE = {
    ADD: 'ADD',
    REMOVE: 'REMOVE'
  };

  SQL_QUERY = {
    LIST_TABLES: 'SELECT name FROM sqlite_master WHERE type = "table"'
  };

  constructor() {
  }
}
