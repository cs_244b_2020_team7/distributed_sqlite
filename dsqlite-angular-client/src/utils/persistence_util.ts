import {PersistenceService, StorageType} from 'angular-persistence';
import {Injectable} from '@angular/core';

@Injectable()
export class PersistenceUtil {

  constructor(private persistenceService: PersistenceService) {
  }

  getClusterName() {
    return this.persistenceService.get('clusterName', StorageType.LOCAL);
  }

  saveClusterName(clusterName) {
    this.persistenceService.set('clusterName', clusterName, {type: StorageType.LOCAL});
  }
}
