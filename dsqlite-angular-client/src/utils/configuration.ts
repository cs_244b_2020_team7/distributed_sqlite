import {Injectable} from '@angular/core';
import {Host} from '../models/Host';

@Injectable()
export class Configuration {
  clusterName: string;
  leader: Host;
  hosts: Host[] = [];
  tables: string[] = [];

  constructor() {}
}
