import {Component, OnInit} from '@angular/core';
import {DsqliteUtil} from '../utils/dsqlite_util';
import {Configuration} from '../utils/configuration';
import {PersistenceUtil} from '../utils/persistence_util';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'dsqlite-angular-client';

  constructor(private dsqliteUtil: DsqliteUtil,
              private config: Configuration,
              private persistenceUtil: PersistenceUtil) { }

  ngOnInit(): void {
    this.config.clusterName = this.persistenceUtil.getClusterName();
    this.dsqliteUtil.fetchCurrentMembers(null);
    this.dsqliteUtil.fetchTablesInDatabase();
  }
}
