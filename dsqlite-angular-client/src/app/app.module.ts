import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {RouterModule} from '@angular/router';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import {ClarityModule} from '@clr/angular';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ConfigureComponent } from './configure/configure.component';
import {FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {Constants} from '../utils/constants';
import {DsqliteUtil} from '../utils/dsqlite_util';
import {Configuration} from '../utils/configuration';
import {PersistenceModule} from 'angular-persistence';
import {PersistenceUtil} from '../utils/persistence_util';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    ConfigureComponent
  ],
  imports: [
    BrowserModule,
    ClarityModule,
    BrowserAnimationsModule,
    HttpClientModule,
    PersistenceModule,
    RouterModule.forRoot([
      {path: '', redirectTo: 'dsqlite', pathMatch: 'full'},
      {path: 'dsqlite', component: HomeComponent},
      // { path: 'products/:productId', component: ProductDetailsComponent },
    ]),
    FormsModule
  ],
  providers: [ Constants, DsqliteUtil, Configuration, PersistenceUtil ],
  bootstrap: [AppComponent]
})
export class AppModule { }
