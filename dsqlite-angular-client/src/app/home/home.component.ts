import { Component, OnInit } from '@angular/core';
import {Constants} from '../../utils/constants';
import {DsqliteUtil} from '../../utils/dsqlite_util';
import {Table} from '../../models/Table';
import {Configuration} from '../../utils/configuration';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  sqlQuery = '';
  readResponse: Table = null;
  updateResponse = '';
  errorMessage = '';

  constructor(private constants: Constants,
              private dsqliteUtil: DsqliteUtil,
              private config: Configuration) { }

  ngOnInit(): void { }

  fetchCurrentMembers() {
    this.dsqliteUtil.fetchCurrentMembers(null);
  }

  fetchTablesInDatabase() {
    this.dsqliteUtil.fetchTablesInDatabase();
  }

  resetResponse() {
    this.readResponse = null;
    this.updateResponse = '';
    this.errorMessage = '';
  }

  executeSqlQuery() {
    this.resetResponse();
    this.dsqliteUtil.executeQuery(this.sqlQuery).subscribe((response: any) => {
        if (response && 'result' in response && response.result) {
          if ('commandType' in response && response.commandType === 'READ') {
            this.readResponse = new Table(JSON.parse(response.result));
          }
          else if ('commandType' in response && response.commandType === 'UPDATE') {
              this.updateResponse = 'SQL query successfully executed. ' + response.result + ' rows modified.';
          }
          else {
            this.errorMessage = 'This sql command type is not supported.';
          }
        }
        else if ('errorMessage' in response && response.errorMessage) {
          this.errorMessage = response.errorMessage;
        }
        else {
          this.errorMessage = 'The server did not return an appropriate response.';
        }
      },
      (error) => {
      if ('errorMessage' in error) {
          this.errorMessage = error.errorMessage;
      }
      });
  }

  get showReadResponse() {
    return (this.readResponse !== null);
  }

  get showUpdateResponse() {
    return (this.updateResponse !== '');
  }

  get showErrorResponse() {
    return (this.errorMessage !== '');
  }

  get showPlaceholder() {
    return (this.readResponse === null && this.updateResponse === '' && this.errorMessage === '');
  }

  get hosts() {
    return this.config.hosts;
  }

  get tables() {
    return this.config.tables;
  }
}
