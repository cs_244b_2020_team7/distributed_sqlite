import {Component, OnInit} from '@angular/core';
import {DsqliteUtil} from '../../utils/dsqlite_util';
import {Configuration} from '../../utils/configuration';
import {PersistenceUtil} from '../../utils/persistence_util';
import {Constants} from '../../utils/constants';
import {Host} from '../../models/Host';

@Component({
  selector: 'app-configure',
  templateUrl: './configure.component.html',
  styleUrls: ['./configure.component.css']
})
export class ConfigureComponent implements OnInit {

  opened = false;
  addClusterError = '';
  showAddHostResponse = false;

  constructor(private config: Configuration,
              private dsqliteUtil: DsqliteUtil,
              private persistenceUtil: PersistenceUtil,
              private constants: Constants) { }

  ngOnInit(): void { }

  openConfigureModal() {
    this.opened = true;
    this.fetchClusterName();
    this.fetchCurrentMembers();
  }

  fetchClusterName() {
    this.config.clusterName = this.persistenceUtil.getClusterName();
  }

  fetchCurrentMembers() {
    this.dsqliteUtil.fetchCurrentMembers(this.showFetchMemberErrorResponse);
  }

  showFetchMemberErrorResponse(error) {
    this.addClusterError = 'Unable to fetch hosts in cluster.';
    this.showAddHostResponse = true;
  }

  addHostToCluster(ip, port) {
    this.resetError();

    if (ip === '' && port === '') {
      this.addClusterError = 'Please provide host IP and port number.';
      this.showAddHostResponse = true;
    }
    else if (ip === '' || port === '') {
      this.addClusterError = (ip === '' ? 'Host IP' : 'Port number') + ' not provided.';
      this.showAddHostResponse = true;
    }
    else {
      this.dsqliteUtil.changeMembers(new Host(ip, port, false), this.constants.MODIFY_MEMBER_TYPE.ADD).subscribe( (response: any) => {
        if ('errorMessage' in response && response.errorMessage) {
          this.addClusterError = response.errorMessage;
          this.showAddHostResponse = true;
        }
        else {
          this.fetchCurrentMembers();
        }
      });
    }
  }

  removeFromCluster(host) {
    this.resetError();
    this.dsqliteUtil.changeMembers(host, this.constants.MODIFY_MEMBER_TYPE.REMOVE).subscribe( (response: any) => {
      if ('errorMessage' in response && response.errorMessage) {
        this.addClusterError = response.errorMessage;
        this.showAddHostResponse = true;
      }
      else {
        this.fetchCurrentMembers();
      }
    });
  }

  saveClusterName(value) {
    this.config.clusterName = value;
    this.persistenceUtil.saveClusterName(value);
    this.fetchCurrentMembers();
  }

  resetError() {
    this.addClusterError = '';
    this.showAddHostResponse = false;
  }

  get hosts() {
    return this.config.hosts;
  }

  get clusterName() {
    return this.config.clusterName;
  }
}
