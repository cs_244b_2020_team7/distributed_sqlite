
export class Host {
  ip: string;
  port: number;
  isLeader: boolean;

  constructor(ip, port, isLeader) {
    this.ip = ip;
    this.port = parseInt(port, 10);
    this.isLeader = isLeader;
  }

  toString() {
    return this.ip + ':' + this.port;
  }
}
