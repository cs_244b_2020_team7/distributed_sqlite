
export class Table {
  columns: string[];
  rows: any[];

  constructor(data) {
    this.columns = [];
    this.rows = [];

    if (data && data.length) {
      for (let i = 0; i < data.length; i++) {
        for (let key in data[i]) {
          if (this.columns.indexOf(key) === -1)
            this.columns.push(key);
        }
        this.rows.push(data[i]);
      }
    }
  }
}
