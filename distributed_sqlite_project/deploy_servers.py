#!/usr/bin/python
import docker
import argparse
import json
import os
import time

print(docker.version)
print(docker.__path__)
parser = argparse.ArgumentParser(description="Deploy sqlite backend service as containers.")
parser.add_argument('--configFile', default='./src/main/resources/configuration.json', help='Path to Raft cluster configuration file.')
parser.add_argument('--groupId', help="Specify groupId for the dsqlite Raft cluster.", required=True)
parser.add_argument('--replica', type=int, help="Specify how many Raft servers (replica) to deploy.", default=3)
parser.add_argument('--port', type=int, help="Default port number for Raft servers.", default=8843)

args = parser.parse_args()
client = docker.from_env()
# Start Raft servers according to specification.
raft_containers = []
for i in range(args.replica):
	c = client.containers.run(image='openjdk:latest', tty=True, name='{}-raft-server-{}'.format(args.groupId, i), detach=True)
	raft_containers.append(c)
# Wait for IP addresses assigned.
endpoints = []
for i in range(len(raft_containers)):
	raft_containers[i] = client.containers.get(raft_containers[i].name)
	while not raft_containers[i].attrs['NetworkSettings'] or not raft_containers[i].attrs['NetworkSettings']['IPAddress']:
		raft_containers[i] = client.containers.get(raft_containers[i].name)
		time.sleep(3)
	c = raft_containers[i]
	print(c.attrs['NetworkSettings']['IPAddress'])
	print(str(args.port))
	endpoints.append('{}:{}'.format(c.attrs['NetworkSettings']['IPAddress'], str(args.port)))
# Add networking info to config file.
configContent = None
with open(args.configFile) as file:
	configContent = json.load(file)
	configs = configContent['allConfigurations']
	updated = False
	for config in configs:
		if config['groupId'] == args.groupId:
			config['groupConfiguration'] = ','.join(endpoints)
			updated = True
	if not updated:
		configs.append({'groupId': args.groupId, 'groupConfiguration': ','.join(endpoints)})
with open(args.configFile, "w") as file:
	file.write(json.dumps(configContent))
# Compile project, copy over the jar, and start the servers.
os.system('mvn clean install -Pnormal_profile')
deployed = []
for c,ip in zip(raft_containers, endpoints):
	os.system('docker cp ./target/distributed_sqlite_project-1.0-SNAPSHOT-jar-with-dependencies.jar {}:/dsqlite.jar'.format(c.short_id))
	print('Depoy raft server:', c, ip)
	javaCmd = 'java -cp ./dsqlite.jar edu.stanford.cs244b.sofajraft.service.SqliteRaftServer /tmp/server {}  {} > samplefile.txt'.format(args.groupId, ip)
	print(javaCmd)
	c.exec_run(cmd=['sh', '-c', javaCmd], detach=True)
	deployed.append((c, ip))
result = {
	'groupId': args.groupId,
	'replica': args.replica,
	'serverPort': args.port,
	'configFile': args.configFile,
	'servers': deployed
}
print(result)


# REPL client container
replclient = client.containers.create(image='openjdk:latest', tty=True, stdin_open=True, name='{}-repl-client'.format(args.groupId), entrypoint=['java', '-cp', './dsqlite.jar', 'edu.stanford.cs244b.sofajraft.client.DistributedSqliteClient', args.groupId])
os.system('docker cp ./target/distributed_sqlite_project-1.0-SNAPSHOT-jar-with-dependencies.jar {}:/dsqlite.jar'.format(replclient.short_id))
print('Creating (not deploying) REPL client')
print(replclient.short_id)

#2PC Coordinator Container
coordinator = client.containers.run(image='openjdk:latest', tty=True, name='2pccoordinator', detach=True)
os.system('docker cp ./target/distributed_sqlite_project-1.0-SNAPSHOT-jar-with-dependencies.jar {}:/dsqlite.jar'.format(coordinator.short_id))
coordinator.exec_run(cmd=['java', '-cp', './dsqlite.jar', 'edu.stanford.cs244b.twopc.DefaultCoordinator'], detach=True)
print('Deploying 2pc coordinator')
print(coordinator.short_id)


# build using spring boot maven plugin
os.system('mvn clean install -Pspringboot_profile')
webserver = client.containers.create(image='openjdk:latest', tty=True, stdin_open=True, name='webserver', ports={'8080/tcp': 8080}, detach=True, entrypoint=['java', '-jar', './dsqlite.jar'])
os.system('docker cp ./target/distributed_sqlite_project-1.0-SNAPSHOT.jar {}:/dsqlite.jar'.format(webserver.short_id))
#webserver.exec_run(cmd=['java', '-jar', './dsqlite.jar'],  detach=True)
webserver.start()
print('Deploying the webserver for REST')
print(webserver.short_id)




