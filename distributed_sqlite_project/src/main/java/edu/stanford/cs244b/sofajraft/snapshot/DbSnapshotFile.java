package edu.stanford.cs244b.sofajraft.snapshot;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Snapshot file to save/load for replicated db states.
 */
public class DbSnapshotFile {

    private static final Logger LOG = LoggerFactory.getLogger(DbSnapshotFile.class);

    private String              path;

    public DbSnapshotFile(String path) {
        super();
        this.path = path;
    }

    public String getPath() {
        return this.path;
    }

    
    public boolean save(final File srcFile) {
        try {
            if (srcFile == null || !srcFile.exists()) {
                return true;
            }
            FileUtils.copyFile(srcFile, new File(this.path));
            try {
                Files.createSymbolicLink(Paths.get(this.path), Paths.get(srcFile.getPath()));
            } catch (FileAlreadyExistsException e) {
                LOG.error("Fail to create symbolic link:", e);
            }
            return true;
        } catch (IOException e) {
            LOG.error("Fail to save snapshot", e);
            return false;
        }
    }

    public void load(String targetFilePath) throws IOException {
        Path path = Paths.get(this.path);
        FileUtils.copyFile(path.toFile(), new File(targetFilePath));
    }
}
