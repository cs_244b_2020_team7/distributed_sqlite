package edu.stanford.cs244b.sofajraft.service;

import com.alipay.sofa.jraft.Status;
import com.alipay.sofa.jraft.closure.ReadIndexClosure;
import com.alipay.sofa.jraft.entity.Task;
import com.alipay.sofa.jraft.error.RaftError;
import com.alipay.sofa.jraft.rhea.StoreEngineHelper;
import com.alipay.sofa.jraft.rhea.options.StoreEngineOptions;
import com.alipay.sofa.jraft.util.BytesUtil;

import edu.stanford.cs244b.sofajraft.jdbc.SQLQueryProcessorImpl;
import edu.stanford.cs244b.sofajraft.jdbc.SqliteJDBC;
import edu.stanford.cs244b.sofajraft.jdbc.SqliteJDBCImpl;
import edu.stanford.cs244b.sofajraft.proto.DatabaseOperationOuterClass.*;
import edu.stanford.cs244b.sofajraft.proto.DatabaseReadRequest.DbReadRequest;
import edu.stanford.cs244b.sofajraft.proto.DbTransactionRequestOuterClass.DbTransactionRequest;
import edu.stanford.cs244b.twopc.proto.DistributedTransactionOuterClass.*;
import edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.*;
import edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.*;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Implementation of sqlite Raft Service
 */
public class SqliteRaftServiceImpl implements SqliteRaftService {
    private static final Logger LOG = LoggerFactory.getLogger(SqliteRaftServiceImpl.class);


    private static final String DATETIMENOW = "datetime('now')";
    private static final String DATETIMENOWLOCALTIME = "datetime('now','localtime')";

    private final SqliteRaftServer sqliteRaftServer;
    private final Executor readIndexExecutor;
    private final String dataPath;
    private final SqliteJDBC sqliteJDBC;
    private String groupId;

    public SqliteRaftServiceImpl(SqliteRaftServer sqliteRaftServer, String dataPath, String groupId) {
        this.sqliteRaftServer = sqliteRaftServer;
        this.readIndexExecutor = createReadIndexExecutor();
        this.dataPath = dataPath;
        this.groupId = groupId;
        this.sqliteJDBC = new SqliteJDBCImpl(this.dataPath);
    }

    private Executor createReadIndexExecutor() {
        final StoreEngineOptions opts = new StoreEngineOptions();
        return StoreEngineHelper.createReadIndexExecutor(opts.getReadIndexCoreThreads());
    }

    public boolean isLeader() {
        return this.sqliteRaftServer.getFsm().isLeader();
    }

    private String getRedirect() {
        return this.sqliteRaftServer.redirect().getRedirect();
    }

    private void readLocal(DbReadRequest dbRequest, DistributedSqliteClosure closure) {
        String result = this.sqliteJDBC.executeRead(dbRequest.getDbName(), dbRequest.getSqlCommand());
        //TODO(estfang) add more check to the result.
        if (result != null) {
            closure.success(result);
            closure.run(Status.OK());
        } else {
            closure.failure("Fail in reading message", "");
            closure.run(Status.OK());
        }
    }

    /**
     * I guess this implementation will go through all the nodes and then return the
     * result by executing the applyOperation.
     * We can get the value from the State machine directly also.
     *
     * @param dbRequest
     * @param closure
     */
    @Override
    public void read(DbReadRequest dbRequest, DistributedSqliteClosure closure) {
        System.out.println(dbRequest.getSqlCommand());
        if (!isLeader()) {
            System.out.println("Not Leader");
            handlerNotLeaderError(closure);
            return;
        }
        switch (dbRequest.getConsistencyLevel()) {
            case WEAK:
                    readLocal(dbRequest, closure);
                break;
            case STRONG:
                this.sqliteRaftServer.getNode().readIndex(BytesUtil.EMPTY_BYTES, new ReadIndexClosure() {
                    @Override
                    public void run(Status status, long l, byte[] bytes) {
                        if (status.isOk()) {
                            readLocal(dbRequest, closure);
                            return;
                        }
                        SqliteRaftServiceImpl.this.readIndexExecutor.execute(() -> {
                            if (isLeader()) {
                                LOG.debug("Fail to get value with 'ReadIndex': {}, try to applying to state machine.",
                                          status);
                                DatabaseOperation dbOperation = DatabaseOperation.newBuilder().setOp(DatabaseOperation.OP.READ)
                                        .setSqlCmd(dbRequest.getSqlCommand())
                                        .setDbName(dbRequest.getDbName())
                                        .build();
                                applyOperation(dbOperation, closure);
                            } else {
                                handlerNotLeaderError(closure);
                            }
                        });
                    }
                });
                break;
            case LOCAL:
                String result = sqliteJDBC.executeRead(dbRequest.getDbName(), dbRequest.getSqlCommand());
                //TODO(estfang) add more check to the result.
                if (result != null) {
                    closure.success(result);
                    closure.run(Status.OK());
                } else {
                    System.out.println("Must be handled otherwise thread will stall"); // TODO remove this
                }
        }

    }

    @Override
    public void update(String dbName, String sqlCmd, DistributedSqliteClosure closure) {
        if (!isLeader()) {
            handlerNotLeaderError(closure);
            return;
        }

        sqlCmd = new SQLQueryProcessorImpl().processNonDeterministicComponents(dbName, sqlCmd, sqliteJDBC);
        System.out.println("Processed SQL command: " + sqlCmd);
        DatabaseOperation dbOperation = DatabaseOperation.newBuilder().setOp(DatabaseOperation.OP.WRITE)
                .setSqlCmd(sqlCmd)
                .setDbName(dbName)
                .build();
        applyOperation(dbOperation, closure);
    }

    private void applyOperation(final DatabaseOperation op, final DistributedSqliteClosure closure) {
        try {
            this.getSqliteRaftServer().recover();
            closure.setDbOperation(op);
            final Task task = new Task();
            task.setData(ByteBuffer.wrap(op.toByteArray()));
            task.setDone(closure);
            this.sqliteRaftServer.getNode().apply(task);
        } catch (Exception e) {
            String errorMsg = "Fail to encode DbOperation";
            LOG.error(errorMsg, e);
            closure.failure(errorMsg, StringUtils.EMPTY);
            closure.run(new Status(RaftError.EINTERNAL, errorMsg));
        }
    }

    @Override
    public void prepareDistTransaction(final String dbName, final String trans_id, final List<String> command,
                                       final DistributedTransaction transaction,
                                       final DistributedSqliteClosure closure) {
        DatabaseOperation dbOperation = DatabaseOperation.newBuilder().setOp(DatabaseOperation.OP.PREPARE)
                .addAllTransQuery(command)
                .setDbName(dbName)
                .setDt(transaction)
                .setDtId(trans_id)
                .build();
        applyOperation(dbOperation, closure);
    }

    @Override
    public void commitDistTransaction(String dbName, String tdId) {
        DistributedTransaction dt = this.getSqliteRaftServer().getParticipantLogKeeper().findTranactionById(tdId);
        if (dt == null) {
            return;
        } else {
            DistributedTransaction.ParticipantInfo dtinfo = this.sqliteRaftServer.getParticipantInfo(dt);

            DatabaseOperation op = DatabaseOperation.newBuilder().setOp(DatabaseOperation.OP.COMMIT)
                    .setDbName(dbName)
                    .addAllTransQuery(dtinfo.getSqlList())
                    .setDtId(tdId)
                    .build();

            final Task task = new Task();
            task.setData(ByteBuffer.wrap(op.toByteArray()));
            this.sqliteRaftServer.getNode().apply(task);
        }
    }

    @Override
    public void abortDistTransaction(String dbName, String tdId) {
        DatabaseOperation op = DatabaseOperation.newBuilder().setOp(DatabaseOperation.OP.ABORT)
                .setDbName(dbName)
                .setDtId(tdId)
                .build();
        final Task task = new Task();
        task.setData(ByteBuffer.wrap(op.toByteArray()));
        this.sqliteRaftServer.getNode().apply(task);
    }

    @Override
    public void updateTransaction(final DbTransactionRequest db_trans_request, final DistributedSqliteClosure closure) {
        DatabaseOperation dbOperation = DatabaseOperation.newBuilder().setOp(DatabaseOperation.OP.UPDATE_TRANS)
                .addAllTransQuery(db_trans_request.getSqlCommandList())
                .setDbName(db_trans_request.getDbName())
                .build();
        applyOperation(dbOperation, closure);
    }


    @Override
    public SqliteRaftServer getSqliteRaftServer() {
        return this.sqliteRaftServer;
    }


    private void handlerNotLeaderError(final DistributedSqliteClosure closure) {
        closure.failure("Not leader.", getRedirect());
        closure.run(new Status(RaftError.EPERM, "Not leader"));
    }
}
