package edu.stanford.cs244b.sofajraft.client;

import com.alipay.sofa.jraft.entity.PeerId;
import com.alipay.sofa.jraft.error.RemotingException;
import com.alipay.sofa.jraft.rhea.util.Lists;
import com.alipay.sofa.jraft.rpc.InvokeCallback;
import com.alipay.sofa.jraft.rpc.impl.cli.CliClientServiceImpl;
import edu.stanford.cs244b.configuration.ConfigurationFactory;
import edu.stanford.cs244b.rest.RpcClientFactory;
import edu.stanford.cs244b.sofajraft.membership.SqliteClusterMembership;
import edu.stanford.cs244b.sofajraft.proto.DatabaseReadRequest.DbReadRequest;
import edu.stanford.cs244b.sofajraft.proto.DatabaseUpdateRequest.DbUpdateRequest;
import edu.stanford.cs244b.sofajraft.proto.DbResponseOuterClass.DbResponse;
import edu.stanford.cs244b.twopc.proto.DecisionMessage.*;
import edu.stanford.cs244b.twopc.proto.DistributedTransactionOuterClass.DistributedTransaction;
import edu.stanford.cs244b.twopc.util.TransactionIdGenerator;

import java.util.List;
import java.util.Scanner;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;

/**
 * Example client code to query sqlite raft server.
 */
public class DistributedSqliteClient {

    private final static String DB_NAME = "test";
    private final static String TABLE_NAME = "warehouses";
    private final static String SELECT_ALL_ROWS_SQL_FORMAT = "SELECT * FROM %s;";
    // Some example commands
    String createTableSql = "CREATE TABLE IF NOT EXISTS warehouses (\n"
            + "	id integer PRIMARY KEY,\n"
            + "	name text NOT NULL,\n"
            + "	capacity real\n"
            + ");";
    String insertItemSql = "INSERT INTO warehouses(name,capacity) VALUES(\"ca\", 100)";
    private static PeerId coId;

    public static void main(final String[] args) throws Exception {
        if (args.length != 1) {
            System.out.println("Usage : java edu.stanford.cs244b.sofajraft.client.DistributedSqliteClient {groupId}");
            System.out.println("Example: edu.stanford.cs244b.sofajraft.client.DistributedSqliteClient dsqlite");
            System.exit(1);
        }
        final String groupId = args[0];

        final CliClientServiceImpl cliClientService = RpcClientFactory.getCliClientService(groupId);
        RpcClientFactory.refreshCurrentLeader(groupId);

        final PeerId leader = RpcClientFactory.getLeader(groupId);
        System.out.println("Leader is " + leader);
        coId = new PeerId();
        coId.parse(ConfigurationFactory.getCoordinatorAddress());
        startScannerLoop(groupId, leader, cliClientService);
    }

    private static void sendDbReadRequest(PeerId leader, CliClientServiceImpl cliClientService,
                                          List<DbReadRequest> dbReadRequests)
            throws InterruptedException, RemotingException {
        System.out.println("DB read request started.");
        final CountDownLatch latch = new CountDownLatch(dbReadRequests.size());
        for (DbReadRequest request : dbReadRequests) {
            cliClientService.getRpcClient().invokeAsync(leader.getEndpoint(), request, new InvokeCallback() {
                @Override
                public void complete(Object o, Throwable throwable) {
                    System.out.println("DB read request result: " + o);
                    try {
                        DbResponse response = (DbResponse) o;
                        System.out.println("Cast response: " + response);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    latch.countDown();
                }

                @Override
                public Executor executor() {
                    return null;
                }
            }, 5000);
        }
        latch.await();
        System.out.println("DB read request finished.");
    }

    private static void sendDbUpdateRequest(PeerId leader, CliClientServiceImpl cliClientService,
                                            List<DbUpdateRequest> dbUpdateRequests)
            throws InterruptedException, RemotingException {
        System.out.println("DB update request started.");
        final CountDownLatch updateLatch = new CountDownLatch(dbUpdateRequests.size());
        for (DbUpdateRequest updateRequest : dbUpdateRequests) {
            cliClientService.getRpcClient().invokeAsync(leader.getEndpoint(), updateRequest, new InvokeCallback() {
                @Override
                public void complete(Object o, Throwable throwable) {
                    System.out.println("DB Update request result " + o);
                    updateLatch.countDown();
                }

                @Override
                public Executor executor() {
                    return null;
                }
            }, 5000);
        }
        updateLatch.await();
        System.out.println("DB update request finished.");
    }

    public static void sendTransactionRequestToCoordinator(CliClientServiceImpl cliClientService, PeerId coid,
                                                           DistributedTransaction request)
            throws InterruptedException, RemotingException {
        System.out.println("Corss DB trans request started.");
        System.out.println(coid.getEndpoint().toString());
        final CountDownLatch updateLatch = new CountDownLatch(1);

        cliClientService.getRpcClient().invokeAsync(coid.getEndpoint(), request, new InvokeCallback() {
            @Override
            public void complete(Object o, Throwable throwable) {
                System.out.println("DB Trans request result: " + o);
                try {
                    Decision response = (Decision) o;
                    System.out.println("Cast response: " + response);
                    updateLatch.countDown();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public Executor executor() {
                return null;
            }
        }, 20000);
        updateLatch.await();
        System.out.println("DB trans request finished.");
    }

    private static DbReadRequest.ConsistencyLevel getConsistencyLevel(String consistency) {
        if (consistency.equals("weak")) {
            return DbReadRequest.ConsistencyLevel.WEAK;
        } else if (consistency.equals("local")) {
            return DbReadRequest.ConsistencyLevel.LOCAL;
        } else {
            return DbReadRequest.ConsistencyLevel.STRONG;
        }
    }

    private static void startScannerLoop(String groupId, PeerId leader, CliClientServiceImpl cliClientService) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("*****Available Options*****");
        System.out.println("Enter quit() to quit the client.");
        System.out.println("Please enter the database name to operate (e.g. test): ");
        String temp = scanner.nextLine();
        if ("quit()".equals(temp)) {
            System.exit(0);
        }
        String dbName = temp;
        while (true) {
            System.out.println("\nEnter any of the below commands: ");
            System.out.println("r - execute read sql query \n" +
                    "w - execute update sql query \n" +
                    "a - add a no∂de to the sqlite cluster \n" +
                    "d - delete a node from the sqlite cluster\n" +
                    "l - list all nodes in cluster\n" +
                    "table - list all tables in current database\n" +
                    "schema - list the schema of the current database\n" +
                    "dt - perform a distributed transaction across multiple raft group\n");
            System.out.println("**********************************");
            String op = scanner.nextLine().toLowerCase();
            switch (op) {
                case "r":
                    System.out.println("Please enter the consistency level");
                    System.out.println("We have 3 consitency level");
                    System.out.println("Weak, Strong, local");
                    System.out.println("Weak and local might return statle data");
                    System.out.println("Enter: Weak");
                    String level = scanner.nextLine().toLowerCase();
                    DbReadRequest.ConsistencyLevel consLevel = getConsistencyLevel(level);
                    System.out.println("Please enter the sql command to read: ");
                    System.out.println("Example: SELECT * FROM warehouses;");
                    String sql = scanner.nextLine();
                    DbReadRequest read_request = DbReadRequest.newBuilder().setDbName(dbName).setSqlCommand(sql).setConsistencyLevel(consLevel).build();

                    try {
                        sendDbReadRequest(leader, cliClientService,
                                Lists.newArrayList(
                                        DbReadRequest.newBuilder().setDbName(dbName).setSqlCommand(sql).build()));
                    } catch (RemotingException e1) {
                        e1.printStackTrace();
                        RpcClientFactory.refreshCurrentLeader(groupId);
                        leader = RpcClientFactory.getLeader(groupId);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "w":
                    System.out.println("Please enter the sql command to update: ");
                    System.out.println("Example: INSERT INTO warehouses(name,capacity) VALUES(\"ca\", 100);");
                    sql = scanner.nextLine();
                    try {
                        sendDbUpdateRequest(leader, cliClientService, Lists.newArrayList(
                                DbUpdateRequest.newBuilder().setDbName(dbName).setSqlCommand(sql).build()));
                    } catch (RemotingException e1) {
                        e1.printStackTrace();
                        RpcClientFactory.refreshCurrentLeader(groupId);
                        leader = RpcClientFactory.getLeader(groupId);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "a":
//                    System.out.println("Please enter the IP of the new node: ");
//                    String newNodeIp = scanner.nextLine();
//                    System.out.println("Please enter the port where the server is running: ");
//                    int newNodePort = Integer.parseInt(scanner.nextLine());
                    String newNodeIp = "";
                    int newNodePort = -1;
                    do {
                        System.out.println("Please enter the IP and port of the new node (Format - IP:port)");
                        String newNode = scanner.nextLine();
                        String[] parts = newNode.split(":");
                        if (parts.length != 2) {
                            System.out.println("Please provide the IP and port in correct format (IP:port)");
                        } else {
                            newNodeIp = parts[0];
                            newNodePort = Integer.parseInt(parts[1]);
                        }
                    } while (newNodeIp.isEmpty() && newNodePort != -1);

                    try {
                        PeerId node = SqliteClusterMembership.addNode(groupId, leader, cliClientService, newNodeIp, newNodePort);
                        if (node != null)
                            System.out.println("Successfully added new node " + node.toString());
                        else
                            System.out.println("Failed to add new node.");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "d":
                    String deleteNodeIp = "";
                    int deleteNodePort = -1;
                    do {
                        System.out.println("Please enter the IP and port of the node to be deleted (Format - IP:port)");
                        String deleteNode = scanner.nextLine();
                        String[] parts = deleteNode.split(":");
                        if (parts.length != 2) {
                            System.out.println("Please provide the IP and port in correct format (IP:port)");
                        } else {
                            deleteNodeIp = parts[0];
                            deleteNodePort = Integer.parseInt(parts[1]);
                        }
                    } while (deleteNodeIp.isEmpty() && deleteNodePort != -1);

                    try {
                        PeerId node = SqliteClusterMembership.deleteNode(groupId, leader, cliClientService, deleteNodeIp, deleteNodePort);
                        if (node != null)
                            System.out.println("Successfully deleted node " + node.toString());
                        else
                            System.out.println("Failed to delete node.");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "l":
                    System.out.println("Nodes in cluster " + groupId);
                    List<PeerId> clusterNodes = SqliteClusterMembership.getNodes(groupId);
                    clusterNodes.stream().forEach(node -> System.out.println(node.toString()));
                    break;
                case "schema":
                    try {
                        sendDbReadRequest(leader, cliClientService,
                                Lists.newArrayList(
                                        DbReadRequest.newBuilder().setDbName(dbName).setSqlCommand("SELECT sql FROM sqlite_master").build()));
                    } catch (RemotingException e1) {
                        e1.printStackTrace();
                        RpcClientFactory.refreshCurrentLeader(groupId);
                        leader = RpcClientFactory.getLeader(groupId);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "dt":
                    System.out.println("The command is for doing atomic updates to multiple raft group: ");
                    System.out.println("Please enter the clusters that you wish to operate");
                    System.out.println("Example: raftgroup1,raftgroup2");
                    String clusterName = scanner.nextLine();
                    String[] cluster_name = clusterName.split(",");
                    int counter = cluster_name.length;
                    DistributedTransaction.Builder transaction_builder = DistributedTransaction.newBuilder()
                            .setId(TransactionIdGenerator.getTransactionId());
                    for (String cluster : cluster_name) {
                        System.out.println("Please enter db name first and queries for each cluster");
                        System.out.println("Example query like this: Start first line with dbname, then the second line for query");
                        System.out.println("dbName");
                        System.out.println("CREATE TABLE foo (id INTEGER NOT NULL PRIMARY KEY, name TEXT)");
                        System.out.println("Enter commit to end query for this cluster");
                        try {
                            DistributedTransaction.ParticipantInfo.Builder info_builder = DistributedTransaction.ParticipantInfo.newBuilder();
                            // Set the name
                            String databaseName = scanner.nextLine().toLowerCase();
                            info_builder.setDbName(databaseName);
                            info_builder.setName(cluster);
                            String next = "";
                            while (true) {
                                next = scanner.nextLine().toLowerCase();
                                if (next.equals("commit")) {
                                    break;
                                }
                                info_builder.addSql(next);
                            }

                            transaction_builder.addParticipants(info_builder.build());
                        } catch (java.lang.Exception e) {
                            e.printStackTrace();
                        }
                    }
                    try {
                        sendTransactionRequestToCoordinator(cliClientService, coId, transaction_builder.build());
                    } catch (RemotingException e1) {
                        e1.printStackTrace();
                        RpcClientFactory.refreshCurrentLeader(groupId);
                        leader = RpcClientFactory.getLeader(groupId);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "table":
                    try {
                        sendDbReadRequest(leader, cliClientService,
                                Lists.newArrayList(
                                        DbReadRequest.newBuilder().setDbName(dbName).setSqlCommand("SELECT name FROM sqlite_master WHERE type ='table'").build()));
                    } catch (RemotingException e1) {
                        e1.printStackTrace();
                        RpcClientFactory.refreshCurrentLeader(groupId);
                        leader = RpcClientFactory.getLeader(groupId);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "quit()":
                    System.exit(0);
                default:
                    System.out.println("Operation not supported.");
                    break;
            }
        }
    }
}
