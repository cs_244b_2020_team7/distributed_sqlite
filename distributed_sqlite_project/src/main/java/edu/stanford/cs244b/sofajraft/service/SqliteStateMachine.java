package edu.stanford.cs244b.sofajraft.service;

import com.alipay.sofa.jraft.Closure;
import com.alipay.sofa.jraft.Iterator;
import com.alipay.sofa.jraft.Status;
import com.alipay.sofa.jraft.core.StateMachineAdapter;
import com.alipay.sofa.jraft.entity.PeerId;
import com.alipay.sofa.jraft.error.RaftError;
import com.alipay.sofa.jraft.error.RaftException;
import com.alipay.sofa.jraft.storage.snapshot.SnapshotReader;
import com.alipay.sofa.jraft.storage.snapshot.SnapshotWriter;
import com.google.protobuf.InvalidProtocolBufferException;
import edu.stanford.cs244b.sofajraft.jdbc.SqliteJDBC;
import edu.stanford.cs244b.sofajraft.jdbc.SqliteJDBCImpl;
import edu.stanford.cs244b.sofajraft.proto.DatabaseOperationOuterClass.DatabaseOperation;
import edu.stanford.cs244b.sofajraft.snapshot.DbSnapshotFile;
import edu.stanford.cs244b.twopc.ParticipantLogKeeper;
import edu.stanford.cs244b.twopc.TransactionService;
import edu.stanford.cs244b.twopc.proto.DecisionMessage.*;
import edu.stanford.cs244b.twopc.proto.DecisionRequest.*;
import edu.stanford.cs244b.twopc.proto.DistributedTransactionOuterClass.*;
import edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.*;
import edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.*;
import edu.stanford.cs244b.twopc.proto.VoteResultOuterClass.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicLong;

import static edu.stanford.cs244b.sofajraft.service.SqliteRaftServer.DB_FOLDER_NAME;

/**
 * State machine to handle individual sqlite server state transition.
 */
public class SqliteStateMachine extends StateMachineAdapter {

    final static String COMMITTED_DB_NAME = "committed";
    final static String CREATE_COMMITTED_TABLE_SQL = "CREATE TABLE IF NOT EXISTS committed (id integer PRIMARY KEY);";
    final static String INSERT_COMMITTED_ITEM_SQL_FORMAT = "INSERT INTO committed(id) VALUES(%d);";
    final static String CHECK_LOG_COMMITTED_SQL_FORMAT = "SELECT 1 FROM committed WHERE id=%d;";


    private static final Logger logger = LoggerFactory.getLogger(SqliteStateMachine.class);
    private TransactionService transactionService;
    private ParticipantLogKeeper participantLogKeeper;
    // Data path for server stable storage.
    private String serverFilePath;
    private String name;

    /**
     * Store the highest logIndex that has been applied to the state machine.
     */
    private final AtomicLong lastAppliedLogIndex = new AtomicLong(-1L);

    public SqliteStateMachine(PeerId serverId, String serverFilePath) {
        this.serverFilePath = serverFilePath;
        this.name = name;
        transactionService = new TransactionService(serverFilePath);
        participantLogKeeper = new ParticipantLogKeeper(serverFilePath);
    }

    /**
     * Leader term
     */
    private final AtomicLong leaderTerm = new AtomicLong(-1);

    public boolean isLeader() {
        return this.leaderTerm.get() > 0;
    }

    @Override
    public void onApply(final Iterator iterator) {
        SqliteJDBC sqliteJDBC = new SqliteJDBCImpl(this.serverFilePath);
        while (iterator.hasNext()) {
            DatabaseOperation dbOperation = null;
            DistributedSqliteClosure closure = (DistributedSqliteClosure) iterator.done();
            if (closure != null) {
                // This task is applied by this node, get value from closure to avoid additional parsing.
                dbOperation = closure.getDbOperation();
            } else {
                // Have to parse FetchAddRequest from this user log.
                final ByteBuffer data = iterator.getData();
                try {
                    dbOperation = DatabaseOperation.parseFrom(data);
                } catch (final InvalidProtocolBufferException e) {
                    logger.error("Fail to decode IncrementAndGetRequest", e);
                }
            }
            if (dbOperation != null) {
                String dbName = dbOperation.getDbName();
                String sqlCommand = dbOperation.getSqlCmd();
                switch (dbOperation.getOp()) {
                    case READ:
                        logger.info("Read db value for sql command: {} at logIndex={}.",
                                dbOperation.getSqlCmd(), iterator.getIndex());
                        String result = sqliteJDBC.executeRead(dbName, sqlCommand);
                        handleClosure(closure, result != null,
                                result, "Unable to serve read request.");
                        break;
                    case WRITE:
                        logger.info("Write db value for sql command: {} at logIndex={} in db {}.",
                                dbOperation.getSqlCmd(), iterator.getIndex(), dbOperation.getDbName());
                        int changed = sqliteJDBC.executeUpdate(dbName, sqlCommand);
                        handleClosure(closure, changed != -1,
                                String.valueOf(changed), "Unable to server update request.");
                        break;
                    case ABORT:
                        logger.info("Index {} , Abort transaction for db {}.", iterator.getIndex(), dbName);

                        final DistributedTransaction transaction = transactionService.findTranactionById(dbOperation.getDtId());
                        if (transaction == null) {
                            logger.info("Iteraor {}, Can't find this transaction {} in the participant", iterator.getIndex(), dbOperation.getDtId());
                            break;
                        } else {
                            TransactionStatus status = TransactionStatus.newBuilder()
                                    .setStatus(TransactionStatus.Status.Abort)
                                    .build();
                            participantLogKeeper.recordMsg(transaction, status);
                        }
                        break;
                    case COMMIT:
                        logger.info("Index {} , commit transaction for db {}.", iterator.getIndex(), dbName);
                        final String transactionId = dbOperation.getDtId();
                        final DistributedTransaction tr = transactionService.findTranactionById(transactionId);
                        if (tr == null) {
                            logger.info("Iteraor {}, Can't find this transaction {} in the participant", iterator.getIndex(), transactionId);
                            break;
                        } else {
                            logger.info("Prepare distributed transaction statement in {} with dbName {}, .", iterator.getIndex(), transactionId);
                            boolean commit = sqliteJDBC.PrepareTransactionDistributionRequest(dbName, dbOperation.getTransQueryList(), true);
                            if (commit) {
                                System.out.println("Commit success");
                            } else {
                                System.out.println("Commit fail");
                            }
                            TransactionStatus status = TransactionStatus.newBuilder()
                                    .setStatus(TransactionStatus.Status.Commit)
                                    .build();
                            participantLogKeeper.recordMsg(tr, status);
                            break;
                        }
                    case PREPARE:
                        participantLogKeeper.saveOrUpdate(dbOperation.getDt());
                        logger.info("Prepare distributed transaction statement in {} with dbName {}, .", iterator.getIndex(), dbName);
                        boolean prepared = sqliteJDBC.PrepareTransactionDistributionRequest(dbName, dbOperation.getTransQueryList(), false);
                        if (prepared) {
                            TransactionStatus vote_status = TransactionStatus.newBuilder()
                                    .setStatus(TransactionStatus.Status.Yes)
                                    .build();
                            logger.info("Prepared!!:");
                            prepared = true;
                            participantLogKeeper.recordMsg(dbOperation.getDt(), vote_status);
                        } else {
                            TransactionStatus vote_status = TransactionStatus.newBuilder()
                                    .setStatus(TransactionStatus.Status.No)
                                    .build();
                            participantLogKeeper.recordMsg(dbOperation.getDt(), vote_status);
                        }
                        handleClosure(closure, prepared,
                                String.valueOf(prepared), "Unable to prepare distributed transaction request.");
                        break;

                }

            }
            iterator.next();
        }
    }


    private void handleClosure(DistributedSqliteClosure closure, boolean succeeded,
                               String result, String errorMsg) {
        logger.info("Sending back response to client.");
        if (closure != null) {
            if (succeeded) {
                closure.success(result);
                closure.run(new Status(200, null));
            } else {
                closure.failure(errorMsg);
                closure.run(new Status(500, errorMsg));
            }
        } else {
            logger.warn("Closure is null");
        }
    }

    @Override
    public void onError(final RaftException e) {
        logger.error("Raft error: {}", e, e);
    }

    @Override
    public void onLeaderStart(final long term) {
        this.leaderTerm.set(term);
        super.onLeaderStart(term);
    }

    @Override
    public void onLeaderStop(final Status status) {
        this.leaderTerm.set(-1);
        super.onLeaderStop(status);
    }

    @Override
    public void onSnapshotSave(SnapshotWriter writer, Closure done) {
        logger.info("=================Creating snapshot.=================");
        File dbFolder = new File(serverFilePath + File.separator + DB_FOLDER_NAME);
        File[] dbFiles = dbFolder.listFiles();
        for (File file : dbFiles) {
            final DbSnapshotFile snapshot = new DbSnapshotFile(writer.getPath() + File.separator + file.getName());
            if (!snapshot.save(file)) {
                done.run(new Status(RaftError.EIO, "Fail to save counter snapshot %s", snapshot.getPath()));
            }
            if (!writer.addFile(file.getName())) {
                done.run(new Status(RaftError.EIO, "Fail to add file to writer"));
            }
        }
        done.run(Status.OK());
    }

    @Override
    public boolean onSnapshotLoad(final SnapshotReader reader) {
        logger.info("=================Loading snapshot.=================");
        if (isLeader()) {
            logger.warn("Leader is not supposed to load snapshot");
            return false;
        }
        File snapshotFolder = new File(reader.getPath());
        File[] snapshotFiles = snapshotFolder.listFiles();
        for (File file : snapshotFiles) {
            if (file.getName().endsWith(SqliteJDBCImpl.DB_SUFFIX)) {
                if (reader.getFileMeta(file.getName()) == null) {
                    logger.error("Fail to find data file in {}", reader.getPath());
                    return false;
                }
                final DbSnapshotFile snapshot = new DbSnapshotFile(file.getPath());
                try {
                    snapshot.load(
                            serverFilePath + File.separator + DB_FOLDER_NAME + File.separator + file.getName());
                } catch (IOException e) {
                    logger.error("Fail to load snapshot {}.", snapshot.getPath(), e);
                }
            }
        }
        logger.info("Successfully load snapshots");
        return true;
    }
}
