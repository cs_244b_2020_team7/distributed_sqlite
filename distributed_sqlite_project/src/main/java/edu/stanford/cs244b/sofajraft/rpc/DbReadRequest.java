package edu.stanford.cs244b.sofajraft.rpc;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

/**
 * Read request to the distributed sqlite database.
 */
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class DbReadRequest implements Serializable {

    private String dbName;

    private static final long serialVersionUID = 9218253805003988803L;

    private String           sqlCmd   = "";

    public String getSqlCmd() {
        return sqlCmd;
    }

    public void setSqlCmd(String sqlCmd) {
        this.sqlCmd = sqlCmd;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }
}
