package edu.stanford.cs244b.sofajraft.rpc;

import com.alipay.sofa.jraft.Status;
import com.alipay.sofa.jraft.rpc.RpcContext;
import com.alipay.sofa.jraft.rpc.RpcProcessor;
import edu.stanford.cs244b.sofajraft.service.DistributedSqliteClosure;
import edu.stanford.cs244b.sofajraft.service.SqliteRaftService;
import edu.stanford.cs244b.twopc.proto.DistributedTransactionOuterClass.*;
import edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.*;
import edu.stanford.cs244b.twopc.proto.VoteOuterClass.*;
import edu.stanford.cs244b.twopc.proto.VoteResultOuterClass.*;

/**
 * Processor for distributed transactional vote request to distributed sqlite.
 */
public class DistributedTransactionProcessor implements RpcProcessor<DistributedTransaction> {

    private final SqliteRaftService sqliteRaftService;
    private final String name;

    public DistributedTransactionProcessor(SqliteRaftService sqliteRaftService, String group_id) {
        super();
        this.sqliteRaftService = sqliteRaftService;
        this.name = group_id;
    }

    @Override
    public void handleRequest(final RpcContext rpcCtx, final DistributedTransaction request) {
        final DistributedSqliteClosure closure = new DistributedSqliteClosure() {
            @Override
            public void run(Status status) {
                if (getDbResponse().getSuccess()) {
                    Vote vote = Vote.newBuilder().setStatus(Vote.Status.Yes).build();
                    VoteResult voteResult = VoteResult.newBuilder()
                            .setVoteResult(vote)
                            .setVoterName(name)
                            .setVoteTime(System.currentTimeMillis())
                            .setTransactionId(request.getId())
                            .build();

                    rpcCtx.sendResponse(voteResult);
                } else {
                    Vote vote = Vote.newBuilder().setStatus(Vote.Status.No).build();
                    VoteResult voteResult = VoteResult.newBuilder()
                            .setVoteResult(vote)
                            .setVoterName(name)
                            .setVoteTime(System.currentTimeMillis())
                            .setTransactionId(request.getId())
                            .build();
                    rpcCtx.sendResponse(voteResult);
                }

            }
        };
        DistributedTransaction.ParticipantInfo pariticpant_info = this.sqliteRaftService.getSqliteRaftServer().getParticipantInfo(request);
        if (pariticpant_info == null) {
            Vote vote = Vote.newBuilder().setStatus(Vote.Status.Unknown).build();
            VoteResult voteResult = VoteResult.newBuilder()
                    .setVoteResult(vote)
                    .setVoterName(name)
                    .setTransactionId(request.getId())
                    .build();
            rpcCtx.sendResponse(voteResult);
        } else {

            this.sqliteRaftService.prepareDistTransaction(pariticpant_info.getDbName(),
                    request.getId(),
                    pariticpant_info.getSqlList(),
                    request,
                    closure);
        }

    }

    @Override
    public String interest() {
        return DistributedTransaction.class.getName();
    }
}
