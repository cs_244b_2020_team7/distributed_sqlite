package edu.stanford.cs244b.sofajraft.rpc;

import com.alipay.sofa.jraft.Status;
import com.alipay.sofa.jraft.rpc.RpcContext;
import com.alipay.sofa.jraft.rpc.RpcProcessor;
import edu.stanford.cs244b.sofajraft.proto.DatabaseReadRequest.DbReadRequest;
import edu.stanford.cs244b.sofajraft.service.DistributedSqliteClosure;
import edu.stanford.cs244b.sofajraft.service.SqliteRaftService;

/**
 * Processor for read request to distributed sqlite.
 */
public class DbReadRequestProcessor implements RpcProcessor<DbReadRequest> {

    private final SqliteRaftService sqliteRaftService;

    public DbReadRequestProcessor(SqliteRaftService sqliteRaftService) {
        super();
        this.sqliteRaftService = sqliteRaftService;
    }

    @Override
    public void handleRequest(final RpcContext rpcCtx, final DbReadRequest request) {
        final DistributedSqliteClosure closure = new DistributedSqliteClosure() {
            @Override
            public void run(Status status) {
                rpcCtx.sendResponse(getDbResponse());
            }
        };

        this.sqliteRaftService.read(request, closure);

    }

    @Override
    public String interest() {
        return DbReadRequest.class.getName();
    }
}
