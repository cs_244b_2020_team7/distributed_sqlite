package edu.stanford.cs244b.sofajraft.rpc;

import com.alipay.sofa.jraft.rpc.RpcContext;
import com.alipay.sofa.jraft.rpc.RpcProcessor;
import edu.stanford.cs244b.sofajraft.service.SqliteRaftService;
import edu.stanford.cs244b.twopc.proto.DecisionMessage.*;
import edu.stanford.cs244b.twopc.proto.DistributedTransactionOuterClass.*;

/**
 * Rpc Processor for decision request to distributed sqlite.
 */

public class DecisionProcessor implements RpcProcessor<Decision> {

    private final SqliteRaftService sqliteRaftService;

    public DecisionProcessor(SqliteRaftService sqliteRaftService) {
        super();
        this.sqliteRaftService = sqliteRaftService;
    }

    @Override
    public void handleRequest(final RpcContext rpcCtx, final Decision request) {
        if (request.getDecision() == Decision.DecisionType.Commit) {
            this.sqliteRaftService.getSqliteRaftServer().commit(request.getTransId());
        } else {
            this.sqliteRaftService.getSqliteRaftServer().abort(request.getTransId());
        }
    }

    @Override
    public String interest() {
        return Decision.class.getName();
    }
}
