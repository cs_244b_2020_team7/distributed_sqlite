package edu.stanford.cs244b.sofajraft.jdbc;

public interface SQLQueryProcessor {
    String processNonDeterministicComponents(String databaseName, String sqlCommand, SqliteJDBC sqliteJDBC);
}
