package edu.stanford.cs244b.sofajraft.jdbc;

import org.json.JSONArray;
import org.json.JSONObject;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SQLQueryProcessorImpl implements SQLQueryProcessor {

    private static final String DATETIME = "datetime\\(('\\w+')(,\\s*'\\w+')*\\)" +
                                            "|date\\(('\\w+')(,\\s*'\\w+')*\\)" +
                                            "|time\\(('\\w+')(,\\s*'\\w+')*\\)" +
                                            "|strftime\\(('\\w+')(,\\s*'\\w+')*\\)";
    private static final String IN_BUILT_ND_FUNCTIONS = "datetime\\(('\\w+')(,\\s*'\\w+')*\\)" +
                                                        "|date\\(('\\w+')(,\\s*'\\w+')*\\)" +
                                                        "|time\\(('\\w+')(,\\s*'\\w+')*\\)" +
                                                        "|strftime\\(('\\w+')(,\\s*'\\w+')*\\)" +
                                                        "|julianday\\(('\\w+')(,\\s*'\\w+')*\\)" +
                                                        "|random\\(\\)";

    private String processLimitFn(String sqlCmd, SqliteJDBC sqliteJDBC) {
        String orignalSqlCmd = new String(sqlCmd);
        sqlCmd = sqlCmd.trim().replaceAll(" +", " ").toLowerCase();
        System.out.println("Original Sql Command - "+ orignalSqlCmd);
        String processedCmd = new String(sqlCmd);

        Matcher matcher = Pattern.compile("(?!order\\s+by\\s+).*limit[ ]+[0-9 ]+").matcher(sqlCmd);

        try {
            while (matcher.find()) {
                System.out.println("Found non-deterministic function " + matcher.group());
                processedCmd = processedCmd.replaceFirst(matcher.group(), "ORDER BY rowid" +  " " +  matcher.group());
                System.out.println(processedCmd);
            }
        } catch(Exception ex){
            System.out.println("Exception thrown while pre-processing limit functions. " + ex);
            return orignalSqlCmd;
        }
        return processedCmd;
    }
    private String generatedOrderByRandomCommand(int count) {
        String value = "ORDER BY\n CASE rowid\n";
        //processedCmd = processedCmd.replaceFirst("order\\s+by\\s+random()", "Where rowid > " +  randvalue + " % abs(" +  value + " );");
        List<Integer> list = new ArrayList<Integer>();
        for (int i = 1; i <= count  + 1; i++){
            list.add(i);
        }
        Collections.shuffle(list);

        for (int i = 1 ; i <= count + 1; i++) {
            //String result = sqliteJDBC.executeRead("", "SELECT random();");
            String whencase = "WHEN " + i + " THEN " + list.get(i-1) + "\n";
            value += whencase;
        }
        value += "END\n";

        return value;
    }
    private String processOrderByWithRandomFn(String databaseName, String sqlCmd, SqliteJDBC sqliteJDBC) {
        String orignalSqlCmd = new String(sqlCmd);
        sqlCmd = sqlCmd.trim().replaceAll(" +", " ").toLowerCase();
        System.out.println("Original Sql Command - "+ orignalSqlCmd);
        String processedCmd = new String(sqlCmd);

        Matcher matcher = Pattern.compile("from((.(?!from))+?)order\\s+by\\s+random.*\\s+").matcher(sqlCmd);

        try {
            while (matcher.find()) {
                System.out.println("Found non-deterministic function " + matcher.group());
                // Try to get the table name
                String tableName = matcher.group().split("\\s+")[1];
                // Get table size
                String result = sqliteJDBC.executeRead(databaseName, "SELECT count() FROM " + tableName + ";");
                JSONArray jsonArray = new JSONArray(result);
                JSONObject jsonObj = jsonArray.getJSONObject(0);
                int value = jsonObj.getInt("count()");
                System.out.println("value is " + value);
                System.out.println("before " +processedCmd);
                processedCmd = processedCmd.replaceFirst("order\\s+by\\s+random\\(\\)", generatedOrderByRandomCommand(value));
                System.out.println("after " +processedCmd);
            }
            System.out.println(processedCmd);
        } catch(Exception ex){
            System.out.println("Exception thrown while pre-processing non deterministic functions. " + ex);
            return orignalSqlCmd;
        }
        return processedCmd;
    }

    @Override
    public String processNonDeterministicComponents(String databaseName, String sqlCmd, SqliteJDBC sqliteJDBC) {
        String orignalSqlCmd = new String(sqlCmd);
        sqlCmd = sqlCmd.trim().replaceAll(" +", " ").toLowerCase();
        System.out.println("Original Sql Command - "+ orignalSqlCmd);
        System.out.println("Trimmed Command - "+ sqlCmd);

        String processedCmd = new String(sqlCmd);
        //processedCmd = processLimitFn(processedCmd, sqliteJDBC);
        processedCmd = processOrderByWithRandomFn(databaseName, processedCmd, sqliteJDBC);
        Matcher matcher = Pattern.compile(IN_BUILT_ND_FUNCTIONS).matcher(sqlCmd);
        while (matcher.find()) {
            System.out.println("Found non-deterministic function " + matcher.group());
            String ndFunction = matcher.group();

            try{
                String result = sqliteJDBC.executeRead(databaseName, "SELECT " + matcher.group() + ";");
                JSONArray jsonArray = new JSONArray(result);
                String value = "";
                JSONObject jsonObj = jsonArray.getJSONObject(0);

                if(matcher.group().matches(DATETIME)) {
                    // If it is a date time function, surround result with single quotes
                    value = "'" + jsonObj.get(ndFunction) + "'";
                }
                else {
                    value = jsonObj.get(ndFunction).toString();
                }
                processedCmd = processedCmd.replaceFirst(IN_BUILT_ND_FUNCTIONS, value);
            }
            catch (Exception ex) {
                System.out.println("Exception thrown while pre-processing non deterministic functions." + ex.getMessage());
                return orignalSqlCmd;
            }
        }
        System.out.println("Final processed command - "+ processedCmd);
        return processedCmd;
    }
}
