package edu.stanford.cs244b.sofajraft.service;

import java.io.Serializable;

/**
 * Define the operations for sqlite DB access.
 */
public class DbOperation implements Serializable {

    private static final long serialVersionUID = -6597003954824547294L;

    /** Database read access */
    public static final byte READ = 0x01;
    /** Database write access */
    public static final byte WRITE = 0x02;

    private byte              op;
    private String            dbName;
    private String            sqlCmd;


    public static DbOperation createReadOp(final String tableName, final String sqlCmd) {
        return new DbOperation(READ, tableName, sqlCmd);
    }

    public static DbOperation createWriteOp(final String tableName, final String sqlCmd) {
        return new DbOperation(WRITE, tableName, sqlCmd);
    }

    public DbOperation(byte op, String dbName, String sqlCmd) {
        this.op = op;
        this.dbName = dbName;
        this.sqlCmd = sqlCmd;
    }

    public byte getOp() {
        return op;
    }

    public String getSqlCmd() {
        return sqlCmd;
    }

    public String getDbName() {
        return dbName;
    }
}
