package edu.stanford.cs244b.sofajraft.rpc;

import com.alipay.sofa.jraft.Status;
import com.alipay.sofa.jraft.rpc.RpcContext;
import com.alipay.sofa.jraft.rpc.RpcProcessor;
import edu.stanford.cs244b.sofajraft.service.DistributedSqliteClosure;
import edu.stanford.cs244b.sofajraft.service.SqliteRaftService;
import edu.stanford.cs244b.sofajraft.proto.DatabaseUpdateRequest.DbUpdateRequest;

/**
 * Rpc Processor for update request to distributed sqlite.
 */
public class DbUpdateRequestProcessor implements RpcProcessor<DbUpdateRequest> {

    private final SqliteRaftService sqliteRaftService;

    public DbUpdateRequestProcessor(SqliteRaftService sqliteRaftService) {
        super();
        this.sqliteRaftService = sqliteRaftService;
    }

    @Override
    public void handleRequest(final RpcContext rpcCtx, final DbUpdateRequest request) {
        final DistributedSqliteClosure closure = new DistributedSqliteClosure() {
            @Override
            public void run(Status status) {
                rpcCtx.sendResponse(getDbResponse());
            }
        };
        this.sqliteRaftService.update(request.getDbName(),
                request.getSqlCommand(),
                closure);
    }

    @Override
    public String interest() {
        return DbUpdateRequest.class.getName();
    }
}
