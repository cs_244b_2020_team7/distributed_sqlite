package edu.stanford.cs244b.sofajraft.rpc;

import com.alipay.sofa.jraft.Status;
import com.alipay.sofa.jraft.rpc.RpcContext;
import com.alipay.sofa.jraft.rpc.RpcProcessor;
import edu.stanford.cs244b.sofajraft.proto.DbTransactionRequestOuterClass.DbTransactionRequest;
import edu.stanford.cs244b.sofajraft.service.DistributedSqliteClosure;
import edu.stanford.cs244b.sofajraft.service.SqliteRaftService;

/**
 * Processor for transactional request to distributed sqlite.
 */
public class DbTransactionRequestProcessor implements RpcProcessor<DbTransactionRequest> {

    private final SqliteRaftService sqliteRaftService;

    public DbTransactionRequestProcessor(SqliteRaftService sqliteRaftService) {
        super();
        this.sqliteRaftService = sqliteRaftService;
    }

    @Override
    public void handleRequest(final RpcContext rpcCtx, final DbTransactionRequest request) {
        final DistributedSqliteClosure closure = new DistributedSqliteClosure() {
            @Override
            public void run(Status status) {
                rpcCtx.sendResponse(getDbResponse());
            }
        };

        this.sqliteRaftService.updateTransaction(request, closure);

    }

    @Override
    public String interest() {
        return DbReadRequest.class.getName();
    }
}
