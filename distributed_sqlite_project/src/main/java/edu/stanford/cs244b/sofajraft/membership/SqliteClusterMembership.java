package edu.stanford.cs244b.sofajraft.membership;

import com.alipay.sofa.jraft.RouteTable;
import com.alipay.sofa.jraft.conf.Configuration;
import com.alipay.sofa.jraft.entity.PeerId;
import com.alipay.sofa.jraft.rpc.CliRequests;
import com.alipay.sofa.jraft.rpc.impl.cli.CliClientServiceImpl;
import com.google.protobuf.Message;

import java.util.ArrayList;
import java.util.List;

public class SqliteClusterMembership {

    public static PeerId addNode(String groupId, PeerId leader, CliClientServiceImpl cliClientService, String ip, int port) {
        try {
            PeerId newNode = new PeerId(ip, port);
            CliRequests.AddPeerRequest addPeerRequest = CliRequests.AddPeerRequest.newBuilder()
                    .setGroupId(groupId)
                    .setLeaderId(leader.toString())
                    .setPeerId(newNode.toString())
                    .build();
            Message message = cliClientService.addPeer(leader.getEndpoint(), addPeerRequest, null).get();
            if (message instanceof CliRequests.AddPeerResponse) {
                final CliRequests.AddPeerResponse result = (CliRequests.AddPeerResponse) message;
                System.out.println("Old Peers");
                for (final String peerIdStr : result.getOldPeersList()) {
                    System.out.println(peerIdStr);
                }
                System.out.println("New Peers");
                for (final String peerIdStr : result.getNewPeersList()) {
                    System.out.println(peerIdStr);
                }
                return newNode;
            }
        } catch (Exception ex) {
            //ex.printStackTrace();
            System.out.println(ex.getMessage());
            System.out.println("Add Node Failed");
        }
        return null;
    }

    public static PeerId deleteNode(String groupId, PeerId leader, CliClientServiceImpl cliClientService, String ip, int port) {
        try {
            PeerId deleteNode = new PeerId(ip, port);
            CliRequests.RemovePeerRequest removePeerRequest = CliRequests.RemovePeerRequest.newBuilder()
                    .setGroupId(groupId)
                    .setLeaderId(leader.toString())
                    .setPeerId(deleteNode.toString())
                    .build();
            Message message = cliClientService.removePeer(leader.getEndpoint(), removePeerRequest, null).get();
            if (message instanceof CliRequests.RemovePeerResponse) {
                final CliRequests.RemovePeerResponse result = (CliRequests.RemovePeerResponse) message;
                System.out.println("Old Peers");
                for (final String peerIdStr : result.getOldPeersList()) {
                    System.out.println(peerIdStr);
                }
                System.out.println("New Peers");
                for (final String peerIdStr : result.getNewPeersList()) {
                    System.out.println(peerIdStr);
                }
                return deleteNode;
            }
        } catch (Exception ex) {
            //ex.printStackTrace();
            System.out.println(ex.getMessage());
            System.out.println("Delete Node Failed");
        }
        return null;
    }

    public static List<PeerId> getNodes(String groupId) {
        Configuration conf = RouteTable.getInstance().getConfiguration(groupId);
        return conf.getPeers();
    }

    public static List<PeerId> getPeers(String groupId, PeerId leader, CliClientServiceImpl cliClientService) {
        CliRequests.GetPeersRequest getPeersRequest = CliRequests.GetPeersRequest.newBuilder()
                .setGroupId(groupId)
                .setLeaderId(leader.toString())
                .setOnlyAlive(true) // All alive nodes
                .build();
        List<PeerId> result = new ArrayList<>();
        try {
            Message message = cliClientService.getPeers(leader.getEndpoint(), getPeersRequest, null).get();
            if (message instanceof CliRequests.GetPeersResponse) {
                CliRequests.GetPeersResponse response = (CliRequests.GetPeersResponse) message;
                for (int i = 0; i < response.getPeersCount(); i++) {
                    result.add(PeerId.parsePeer(response.getPeers(i)));
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }
}
