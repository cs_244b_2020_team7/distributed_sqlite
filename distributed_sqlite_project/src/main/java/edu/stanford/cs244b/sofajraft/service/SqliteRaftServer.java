package edu.stanford.cs244b.sofajraft.service;

import com.alipay.sofa.jraft.Node;
import com.alipay.sofa.jraft.RaftGroupService;
import com.alipay.sofa.jraft.conf.Configuration;
import com.alipay.sofa.jraft.entity.PeerId;
import com.alipay.sofa.jraft.option.NodeOptions;
import com.alipay.sofa.jraft.rpc.RaftRpcServerFactory;
import com.alipay.sofa.jraft.rpc.RpcServer;
import edu.stanford.cs244b.configuration.ConfigurationFactory;
import edu.stanford.cs244b.sofajraft.proto.DbResponseOuterClass.*;
import edu.stanford.cs244b.sofajraft.rpc.DbReadRequestProcessor;
import edu.stanford.cs244b.sofajraft.rpc.DbUpdateRequestProcessor;
import edu.stanford.cs244b.sofajraft.rpc.DecisionProcessor;
import edu.stanford.cs244b.sofajraft.rpc.DistributedTransactionProcessor;
import edu.stanford.cs244b.twopc.AbstractParticipant;
import edu.stanford.cs244b.twopc.TransactionService;
import edu.stanford.cs244b.twopc.proto.DistributedTransactionOuterClass.*;
import edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.*;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * sqlite Raft server implementation.
 */
public class SqliteRaftServer extends AbstractParticipant {

    private RaftGroupService raftGroupService;
    private Node node;
    private SqliteStateMachine fsm;
    private TransactionService transactionService;
    private SqliteRaftService sqliteRaftService;
    private String name;
    private String groupId;
    public static final String DB_FOLDER_NAME = "sqlite-db";

    public SqliteRaftServer(final String dataPath, final String groupId, final PeerId serverId,
                            final NodeOptions nodeOptions) throws IOException {
        super(dataPath, serverId.getEndpoint().toString(), ConfigurationFactory.getCoordinatorAddress());
        this.groupId = groupId;
        // Init file path. I move it to parent
       // FileUtils.Mkdir(new File(dataPath));

        // Init db data folder
        //FileUtils.Mkdir(new File(dataPath + File.separator + DB_FOLDER_NAME));
        // Raft RPC and business RPC share the same.
        final RpcServer rpcServer = RaftRpcServerFactory.createRaftRpcServer(serverId.getEndpoint());
        // Register business logic processor.
        sqliteRaftService = new SqliteRaftServiceImpl(this, dataPath, groupId);
        rpcServer.registerProcessor(new DbReadRequestProcessor(sqliteRaftService));
        rpcServer.registerProcessor(new DbUpdateRequestProcessor(sqliteRaftService));
        rpcServer.registerProcessor(new DistributedTransactionProcessor(sqliteRaftService, groupId));
        rpcServer.registerProcessor(new DecisionProcessor(sqliteRaftService));

        // Init state machine.
        this.fsm = new SqliteStateMachine(serverId, dataPath);

        nodeOptions.setFsm(this.fsm);
        // Stable storage setup
        // log is required.
        nodeOptions.setLogUri(dataPath + File.separator + "log");
        // meta data is required.
        nodeOptions.setRaftMetaUri(dataPath + File.separator + "raft_meta");
        // snapshot is optional.
        nodeOptions.setSnapshotUri(dataPath + File.separator + "snapshot");
        transactionService = new TransactionService(dataPath);

        // Start up Raft cluster service.
        this.raftGroupService = new RaftGroupService(groupId, serverId, nodeOptions, rpcServer);
        // Start up individual Raft node.
        this.node = this.raftGroupService.start();
    }


    public SqliteRaftService getService() {
        return this.sqliteRaftService;
    }

    public SqliteStateMachine getFsm() {
        return this.fsm;
    }

    public Node getNode() {
        return this.node;
    }

    public RaftGroupService RaftGroupService() {
        return this.raftGroupService;
    }

    /**
     * Redirect request to new leader
     */
    public DbResponse redirect() {
        String leader_id = "";
        if (this.node != null) {
            final PeerId leader = this.node.getLeaderId();
            if (leader != null) {
                leader_id = leader.toString();
            }
        }
        DbResponse response = DbResponse.newBuilder()
                .setRedirect(leader_id)
                .setSuccess(false)
                .build();
        return response;
    }

    public DistributedTransaction.ParticipantInfo getParticipantInfo(DistributedTransaction transaction) {
        for (DistributedTransaction.ParticipantInfo info : transaction.getParticipantsList()) {
            if (info.getName().equals(groupId)) {
                return info;
            }
        }
        return null;
    }

    @Override
    public void abort(String transactionId) {
        final DistributedTransaction transaction = transactionService.findTranactionById(transactionId);
        if (transaction == null) {
            System.out.println("Can't find this transaction in the participant"
                    + transactionId);
            return;
        }
        DistributedTransaction.ParticipantInfo participantInfo = getParticipantInfo(transaction);
        if (participantInfo == null) {
            System.err.println("No matching info for participant info");
            return;
        }
        try {
            String dbName = participantInfo.getDbName();
            this.getService().abortDistTransaction(dbName, transaction.getId());
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void commit(String transactionId) {
        final DistributedTransaction transaction = transactionService.findTranactionById(transactionId);
        if (transaction == null) {
            System.out.println("Can't find this transaction in the participant"
                    + transactionId);
            return;
        }
        DistributedTransaction.ParticipantInfo participantInfo = getParticipantInfo(transaction);
        if (participantInfo == null) {
            System.out.println("Can't find this transaction  in the participant"
                    + transactionId);
            return;
        }

        try {
            this.getService().commitDistTransaction(participantInfo.getDbName(), transaction.getId());
        } catch (java.lang.Exception e) {
            e.printStackTrace();
        }
    }


    public static void main(final String[] args) throws IOException {
        if (args.length != 3) {
            System.out
                    .println("Useage : java edu.stanford.cs244b.sofajraft.service.SqliteRaftServer {dataPath} {groupId} {serverId}");
            System.out
                    .println("Example: java edu.stanford.cs244b.sofajraft.service.SqliteRaftServer /tmp/server1 dsqlite 127.0.0.1:8443");
            System.exit(1);
        }
        final String dataPath = args[0];
        System.out.println(dataPath);
        final String groupId = args[1];
        final String serverIdStr = args[2];
        final String initConfStr = ConfigurationFactory.getConfiguration(groupId);
        System.out.println("Conf string is "+ initConfStr);
        final NodeOptions nodeOptions = new NodeOptions();
        // leader election timeout
        nodeOptions.setElectionTimeoutMs(1000);
        // disable cli service.
        nodeOptions.setDisableCli(false);
        // snapshot interval.
        nodeOptions.setSnapshotIntervalSecs(30);
        // validation for sever address
        final PeerId serverId = new PeerId();
        if (!serverId.parse(serverIdStr)) {
            throw new IllegalArgumentException("Fail to parse serverId:" + serverIdStr);
        }
        final Configuration initConf = new Configuration();
        if (!initConf.parse(initConfStr)) {
            throw new IllegalArgumentException("Fail to parse initConf:" + initConfStr);
        }

        // set up initial config for Raft cluster.
        nodeOptions.setInitialConf(initConf);

        // start up the Raft server.
        final SqliteRaftServer sqliteRaftServer = new SqliteRaftServer(dataPath, groupId, serverId, nodeOptions);
        System.out.println("Started sqlite raft server at port:"
                + sqliteRaftServer.getNode().getNodeId().getPeerId().getPort());
    }
}
