package edu.stanford.cs244b.sofajraft.jdbc;

import static edu.stanford.cs244b.sofajraft.service.SqliteRaftServer.DB_FOLDER_NAME;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.function.Function;

import edu.stanford.cs244b.twopc.proto.DistributedTransactionOuterClass.*;

public class SqliteJDBCImpl implements SqliteJDBC {
    public final static String DB_SUFFIX = ".db";
    private final static ResultSetJsonExtractor resultSetJsonExtractor = new ResultSetJsonExtractor();

    private String serverFilePath;
    private String dbDataFolder;

    public SqliteJDBCImpl(String serverFilePath) {
        this.serverFilePath = serverFilePath;
        this.dbDataFolder = String.format("jdbc:sqlite:%s", serverFilePath + File.separator + DB_FOLDER_NAME);
    }

    @Override
    public String executeRead(String databaseName, String sqlCommand) {
        System.out.println("Executing sql read command: " + sqlCommand);
        String result = "error";
        Connection connection = null;
        System.out.println(databaseName);
        System.out.println(sqlCommand);

        try {
            // create a database connection
            connection = DriverManager.getConnection(this.dbDataFolder + File.separator + databaseName + DB_SUFFIX);
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            ResultSet rs = statement.executeQuery(sqlCommand);
            result = resultSetJsonExtractor.extractData(rs).toString();
        } catch (Exception e) {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.out.println(e.getMessage());

            System.err.println(e.getMessage());
            return null;
        } finally {
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                // connection close failed.
                System.err.println(e.getMessage());
                return null;
            }
        }
        System.out.println(result);
        return result;
    }

    @Override
    public int executeUpdate(String databaseName, String sqlCommand) {
        System.out.println("Executing sql update command");
        int rowCount = -1;

        Connection connection = null;
        try {
            // create a database connection
            connection = DriverManager.getConnection(this.dbDataFolder + File.separator + databaseName + DB_SUFFIX);
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.
            rowCount = statement.executeUpdate(sqlCommand);
        } catch (SQLException e) {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.err.println(e.getMessage());
        } finally {
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                // connection close failed.
                System.err.println(e.getMessage());
            }
        }
        return rowCount;
    }

    public boolean PrepareTransactionDistributionRequest(String databaseName, List<String> sqlCommands, Boolean commit) {
        System.out.println("Executing sql update command with transaction");
        int rowCount = -1;
        Connection connection = null;
        try {
            // create a database connection
            connection = DriverManager.getConnection(this.dbDataFolder + File.separator + databaseName + DB_SUFFIX);
            connection.setAutoCommit(commit);
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.
            for (String sql : sqlCommands) {
                statement.addBatch(sql);
            }
            statement.executeBatch();
            connection.close();
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public String executeReadTrans(String databaseName, String sqlCommand, long logIndex,
                                   Function<Long, Void> onLogApplied) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int executeUpdateTrans(String databaseName, String sqlCommand,
                                  long logIndex, Function<Long, Void> onLogApplied) {
        System.out.println("Executing sql update command with transaction");
        int rowCount = -1;

        Connection connection = null;
        try {
            // create a database connection
            connection = DriverManager.getConnection(this.dbDataFolder + File.separator + databaseName + DB_SUFFIX);
            connection.setAutoCommit(false);

            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.
            rowCount = statement.executeUpdate(sqlCommand);
            // include last applied index in transaction.
            onLogApplied.apply(logIndex);
            connection.commit();
        } catch (SQLException e) {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.err.println(e.getMessage());
            // rollback
            if (connection != null) {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    System.err.println(e1.getMessage());
                }
            }
        } finally {
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                // connection close failed.
                System.err.println(e.getMessage());
            }
        }
        return rowCount;
    }


    @Override
    public boolean executeUpdateTransaction(String databaseName, List<String> sqlCommands) {
        System.out.println("Executing sql bulk  update command");
        Connection connection = null;
        boolean success = false;
        try {
            // create a database connection
            connection = DriverManager.getConnection(this.dbDataFolder + File.separator + databaseName + DB_SUFFIX);
            connection.setAutoCommit(false);
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.
            for (String cmd : sqlCommands) {
                statement.addBatch(cmd);
            }
            statement.executeBatch();
        } catch (SQLException e) {
            try {
                // Rollback the current transaction and print out the error message.
                connection.rollback();
            } catch (SQLException ex) {
                System.err.println(ex.getMessage());
            }
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.err.println(e.getMessage());
        } finally {
            try {
                if (connection != null) {
                    connection.commit();

                    connection.close();
                    success = true;
                }
            } catch (SQLException e) {
                // connection close failed.
                success = false;

                System.err.println(e.getMessage());
            }
        }
        return success;
    }
}
