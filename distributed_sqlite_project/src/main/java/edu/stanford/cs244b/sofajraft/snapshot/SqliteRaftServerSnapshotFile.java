package edu.stanford.cs244b.sofajraft.snapshot;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Snapshot file for Raft server to avoid excessive replay after restart.
 * TODO: need to modify for sqlite database.
 */
public class SqliteRaftServerSnapshotFile {

    private static final Logger LOG = LoggerFactory.getLogger(SqliteRaftServerSnapshotFile.class);

    private String              path;

    public SqliteRaftServerSnapshotFile(String path) {
        super();
        this.path = path;
    }

    public String getPath() {
        return this.path;
    }

    /**
     * Save all db files as soft links.
     */
    public List<String> save() throws IOException {
        List<String> savedFiles = new ArrayList<>();
        File dir = new File(".");
        File[] dbFiles = dir.listFiles((d, name) -> name.endsWith(".db"));
        for (File dbFile : dbFiles) {
            Path link = Paths.get(this.path, dbFile.getName());
            if (Files.exists(link)) {
                Files.delete(link);
            }
            Files.createSymbolicLink(link, Paths.get(dbFile.getPath()));
            savedFiles.add(dbFile.getName());
        }
        return savedFiles;
    }

    /**
     *
     * @return
     * @throws IOException
     */
    public void load() throws IOException {
        File dir = new File(this.path);
        File[] dbFiles = dir.listFiles((d, name) -> name.endsWith(".db"));
        for (File dbFile : dbFiles) {
            FileUtils.copyFile(dbFile, new File("." + File.separator + dbFile.getName()));
        }
    }
}
