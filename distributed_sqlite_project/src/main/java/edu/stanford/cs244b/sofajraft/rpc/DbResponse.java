package edu.stanford.cs244b.sofajraft.rpc;

import java.io.Serializable;

import lombok.Builder;
import lombok.ToString;

/**
 * Response from distributed sqlite to requests.
 */
@ToString
@Builder
public class DbResponse implements Serializable {

    private static final long serialVersionUID = -4220017686727146773L;

    private String            result;

    private boolean           success;

    /**
     * redirect peer id
     */
    private String            redirect;

    private String            errorMsg;

    public String getErrorMsg() {
        return this.errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getRedirect() {
        return this.redirect;
    }

    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }

    public boolean isSuccess() {
        return this.success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public DbResponse(String result, boolean success, String redirect, String errorMsg) {
        super();
        this.result = result;
        this.success = success;
        this.redirect = redirect;
        this.errorMsg = errorMsg;
    }

    public DbResponse() {
        super();
    }
}
