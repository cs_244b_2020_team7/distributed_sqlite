package edu.stanford.cs244b.sofajraft.jdbc;

import java.util.function.Function;
import java.util.List;
import java.util.ArrayList;
import edu.stanford.cs244b.twopc.proto.DistributedTransactionOuterClass.*;

public interface SqliteJDBC {
    String executeRead(String databaseName, String sqlCommand);
    int executeUpdate(String databaseName, String sqlCommand);
    String executeReadTrans(String databaseName, String sqlCommand, long logIndex, Function<Long, Void> onLogApplied);
    int executeUpdateTrans(String databaseName, String sqlCommand, long logIndex, Function<Long, Void> onLogApplied);
    boolean executeUpdateTransaction(String databaseName, List<String> sqlCommands);
    boolean PrepareTransactionDistributionRequest(String databaseName, List<String> sqlCommands,  Boolean commit);
}
