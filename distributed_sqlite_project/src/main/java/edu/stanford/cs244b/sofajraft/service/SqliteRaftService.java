package edu.stanford.cs244b.sofajraft.service;

import edu.stanford.cs244b.sofajraft.proto.DatabaseReadRequest.DbReadRequest;
import edu.stanford.cs244b.sofajraft.proto.DbTransactionRequestOuterClass.DbTransactionRequest;
import edu.stanford.cs244b.twopc.proto.DistributedTransactionOuterClass.*;
import edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.*;

import java.util.List;

/**
 * Service distributed sqlite service read/write requests.
 */
public interface SqliteRaftService {

    // Read query to sqlite Raft cluster.
    void read(final DbReadRequest db_read_request, final DistributedSqliteClosure closure);

    // Transaction Update query to sqlite Raft cluster.
    void updateTransaction(final DbTransactionRequest db_trans_request, final DistributedSqliteClosure closure);
    SqliteRaftServer getSqliteRaftServer();
    /**
     * Update request to the sqlite raft server
     * Example : CREATE, ALTER commands
     * @param sqlCmd
     * @param closure
     */
    void update(final String dbName, final String sqlCmd, final DistributedSqliteClosure closure);

    void abortDistTransaction(final String dbName, final String transactionId);
    void commitDistTransaction(final String dbName, final String transactionId);
    void prepareDistTransaction(final String dbName, final String trans_id, final List<String> command,
                                final DistributedTransaction transaction,
                                final DistributedSqliteClosure closure);
}