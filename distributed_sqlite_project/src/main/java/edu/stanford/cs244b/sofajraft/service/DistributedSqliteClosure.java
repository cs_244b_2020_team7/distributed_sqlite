package edu.stanford.cs244b.sofajraft.service;

import com.alipay.sofa.jraft.Closure;
import edu.stanford.cs244b.sofajraft.proto.DatabaseOperationOuterClass.*;
import edu.stanford.cs244b.sofajraft.proto.DbResponseOuterClass.*;
/**
 * Closure as callback for state update success or failure.
 */
public abstract class DistributedSqliteClosure implements Closure {
    private DbResponse dbResponse;
    private DatabaseOperation dbOperation;

    public void setDbOperation(DatabaseOperation dbOperation) {
        this.dbOperation = dbOperation;
    }

    public DatabaseOperation getDbOperation() {
        return dbOperation;
    }

    public DbResponse getDbResponse() {
        return dbResponse;
    }

    public void setDbResponse(DbResponse dbResponse) {
        this.dbResponse = dbResponse;
    }

    protected void failure(final String errorMsg, final String redirect) {
        DbResponse response = DbResponse.newBuilder()
                .setSuccess(false)
                .setErrorMsg(errorMsg)
                .setRedirect(redirect)
                .build();
        setDbResponse(response);
    }

    protected void failure(final String errorMsg) {
        DbResponse response = DbResponse.newBuilder()
                .setSuccess(false)
                .setErrorMsg(errorMsg)
                .build();
        setDbResponse(response);
    }

    /*
    result parameter should not be null, otherwise it will get stuck
     */
    protected void success(final String result) {
        DbResponse response = DbResponse.newBuilder()
                                        .setResult(result)
                                        .setSuccess(true)
                                        .build();
        setDbResponse(response);
    }

}
