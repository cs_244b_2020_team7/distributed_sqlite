package edu.stanford.cs244b.sofajraft.rpc;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;

/**
 * Update request to the distributed sqlite database.
 */
@AllArgsConstructor
@Builder
public class DbUpdateRequest implements Serializable {

    private String dbName;

    private String sqlCommand;

    /* serialVersionUID should be unique and fixed for each Serializable
     * class.
     */
    private static final long serialVersionUID = 8218253805003988803L;

    private String sqlCmd = "";

    public String getSqlCmd() {
        return sqlCmd;
    }

    public void setSqlCmd(String sqlCmd) {
        this.sqlCmd = sqlCmd;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }
}
