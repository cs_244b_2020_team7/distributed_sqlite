package edu.stanford.cs244b.configuration;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.*;
import java.nio.file.FileAlreadyExistsException;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;


public class ConfigurationFactory {
    private static volatile Map<String, String> configMap = new ConcurrentHashMap<>();
    private static String masterAddress;
    private ConfigurationFactory(){
        // read from configuration file
        try {
            InputStream is = this.getClass().getResourceAsStream("/configuration.json");
            JSONTokener jsonTokener = new JSONTokener(is);
            JSONObject jsonObject = new JSONObject(jsonTokener);
            JSONArray jsonArray = jsonObject.getJSONArray("allConfigurations");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                String groupId = jsonObject1.getString("groupId");
                String config = jsonObject1.getString("groupConfiguration");
                configMap.put(groupId, config);
            }
            JSONObject master = jsonObject.getJSONObject("coordinatorId");
            masterAddress = master.getString("address");
            System.out.println("Loaded Config from file; Size : " + configMap.size());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static String getCoordinatorAddress(){
        if (masterAddress == null){
            new ConfigurationFactory();
        }
        return masterAddress;
    }
    public static Set<String> getAllParticipants(){
        return configMap.keySet();
    }

    public static synchronized String getConfiguration(String groupId) {
        if (configMap.isEmpty()) {
           new ConfigurationFactory();
        }
        if (configMap.containsKey(groupId)) {
            return configMap.get(groupId);
        }
        System.out.println("No configuration was found for  groupId " + groupId);
        return null;
    }


}
