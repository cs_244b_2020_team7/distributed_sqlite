package edu.stanford.cs244b.pojo;

public class RaftPeerInfo {
    private String ip;
    private String port;
    private String groupId;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    @Override
    public String toString() {
        return "RaftPeerInfo{" +
                "ip='" + ip + '\'' +
                ", port='" + port + '\'' +
                ", groupId='" + groupId + '\'' +
                '}';
    }
}
