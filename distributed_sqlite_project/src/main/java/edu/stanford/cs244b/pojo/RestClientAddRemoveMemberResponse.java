package edu.stanford.cs244b.pojo;

public class RestClientAddRemoveMemberResponse {
    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getLeaderAddress() {
        return leaderAddress;
    }

    public void setLeaderAddress(String leaderAddress) {
        this.leaderAddress = leaderAddress;
    }

    public RaftPeerInfo getRaftPeerInfo() {
        return raftPeerInfo;
    }

    public void setRaftPeerInfo(RaftPeerInfo raftPeerInfo) {
        this.raftPeerInfo = raftPeerInfo;
    }
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }


    @Override
    public String toString() {
        return "RestClientAddRemoveMemberResponse{" +
                "statusCode='" + statusCode + '\'' +
                ", action='" + action + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", leaderAddress='" + leaderAddress + '\'' +
                ", raftPeerInfo=" + raftPeerInfo +
                '}';
    }

    private String statusCode;
    private String action; // "ADD" or "REMOVE"
    private String errorMessage;
    private String leaderAddress;
    private RaftPeerInfo raftPeerInfo;

}
