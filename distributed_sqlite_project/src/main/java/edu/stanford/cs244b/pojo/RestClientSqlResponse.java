package edu.stanford.cs244b.pojo;

public class RestClientSqlResponse {

	// Http Status code
	private String statusCode;
	private String errorMessage;
	private String commandType; // "READ" or "UPDATE"
	// we should always tell client about the group and leader
	private String groupId;
	private String leaderAddress;
	private String result;


	public String getCommandType() {
		return commandType;
	}

	public void setCommandType(String commandType) {
		this.commandType = commandType;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getLeaderAddress() {
		return leaderAddress;
	}
	public void setLeaderAddress(String leaderAddress) {
		this.leaderAddress = leaderAddress;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}


	@Override
	public String toString() {
		return "RestClientSqlResponse{" +
				"statusCode='" + statusCode + '\'' +
				", errorMessage='" + errorMessage + '\'' +
				", commandType='" + commandType + '\'' +
				", groupId='" + groupId + '\'' +
				", leaderAddress='" + leaderAddress + '\'' +
				", result='" + result + '\'' +
				'}';
	}
}
