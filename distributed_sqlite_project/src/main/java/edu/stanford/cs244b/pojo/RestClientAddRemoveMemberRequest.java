package edu.stanford.cs244b.pojo;

public class RestClientAddRemoveMemberRequest {
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }


    @Override
    public String toString() {
        return "RestClientAddRemoveMemberRequest{" +
                "ip='" + ip + '\'' +
                ", port='" + port + '\'' +
                ", groupId='" + groupId + '\'' +
                ", action='" + action + '\'' +
                '}';
    }

    private String ip;
    private String port;
    private String groupId;
    private String action; // "ADD" or "REMOVE"

}
