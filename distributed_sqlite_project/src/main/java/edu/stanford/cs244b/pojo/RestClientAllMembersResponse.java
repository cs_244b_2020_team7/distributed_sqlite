package edu.stanford.cs244b.pojo;

import java.util.List;

public class RestClientAllMembersResponse {
    private String statusCode;
    private String errorMessage;
    private String leaderAddress;
    private String groupId;
    private List<RaftPeerInfo> peerInfoList;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getLeaderAddress() {
        return leaderAddress;
    }

    public void setLeaderAddress(String leaderAddress) {
        this.leaderAddress = leaderAddress;
    }

    public List<RaftPeerInfo> getPeerInfoList() {
        return peerInfoList;
    }

    public void setPeerInfoList(List<RaftPeerInfo> peerInfoList) {
        this.peerInfoList = peerInfoList;
    }

    public String getGroupId() { return groupId; }

    public void setGroupId(String groupId) { this.groupId = groupId; }

    @Override
    public String toString() {
        return "RestClientAllMembersResponse{" +
                "statusCode='" + statusCode + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", leaderAddress='" + leaderAddress + '\'' +
                ", groupId='" + groupId + '\'' +
                ", peerInfoList=" + peerInfoList +
                '}';
    }
}
