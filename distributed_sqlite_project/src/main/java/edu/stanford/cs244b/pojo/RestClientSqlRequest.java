package edu.stanford.cs244b.pojo;

public class RestClientSqlRequest {
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public String getSqlCommand() {
		return sqlCommand;
	}

	public void setSqlCommand(String sqlCommand) {
		this.sqlCommand = sqlCommand;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	@Override
	public String toString() {
		return "RestClientSqlRequest{" +
				"id='" + id + '\'' +
				", groupId='" + groupId + '\'' +
				", dbName='" + dbName + '\'' +
				", sqlCommand='" + sqlCommand + '\'' +
				'}';
	}

	private String id;
	private String groupId;
	private String dbName;
	private String sqlCommand;

}
