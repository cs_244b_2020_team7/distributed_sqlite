package edu.stanford.cs244b.rest;

import com.alipay.sofa.jraft.RouteTable;
import com.alipay.sofa.jraft.conf.Configuration;
import com.alipay.sofa.jraft.entity.PeerId;
import com.alipay.sofa.jraft.option.CliOptions;
import com.alipay.sofa.jraft.rpc.impl.cli.CliClientServiceImpl;
import edu.stanford.cs244b.configuration.ConfigurationFactory;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeoutException;

public class RpcClientFactory {
    private static volatile CliClientServiceImpl cliClientService;
    private static volatile ConcurrentHashMap<String,PeerId>  leaderMap;

    // private constructor for Singelton
    private RpcClientFactory() {}

    public static synchronized boolean refreshCurrentLeader(String groupId){
        try {
            if (!RouteTable.getInstance().refreshLeader(cliClientService, groupId, 5000).isOk()) {
                System.out.println("Failed to refresh leader");
                return false;
            }
        } catch (InterruptedException ex) {
            System.out.println("thread was interrupted");
        } catch (TimeoutException ex) {
            System.out.println("Timeout happened");
        }
        PeerId leader = RouteTable.getInstance().selectLeader(groupId);
        System.out.println("Refreshed Leader and put into map -" + leader.toString());
        leaderMap.put(groupId, leader);
        return true;
    }

    public static CliClientServiceImpl getCliClientService(String groupId){
        if (cliClientService == null) {
            synchronized (RpcClientFactory.class) {
                if (cliClientService == null ) {
                    //  confStr looks like "127.0.0.1:8443,127.0.0.1:8444,127.0.0.1:8445";
                    final String confStr = ConfigurationFactory.getConfiguration(groupId);
                    final Configuration conf = new Configuration();
                    if (confStr == null || !conf.parse(confStr)) {
                        System.out.println("Failed to parse the Configuration String - " + confStr);
                        return null;
                    }
                    RouteTable.getInstance().updateConfiguration(groupId, conf);
                    cliClientService = new CliClientServiceImpl();
                    cliClientService.init(new CliOptions());
                    leaderMap = new ConcurrentHashMap<>();
                    System.out.println("RpcClientFactory Initialization successful");
                }
            }
        }
        return cliClientService;
    }

    public  static PeerId getLeader(String groupId) {
       return leaderMap.getOrDefault(groupId, null);
    }
}
