package edu.stanford.cs244b.rest;

import com.alipay.sofa.jraft.entity.PeerId;
import com.alipay.sofa.jraft.rpc.InvokeCallback;
import com.alipay.sofa.jraft.rpc.impl.cli.CliClientServiceImpl;

import edu.stanford.cs244b.pojo.*;
import edu.stanford.cs244b.sofajraft.membership.SqliteClusterMembership;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


import edu.stanford.cs244b.sofajraft.proto.DatabaseReadRequest.DbReadRequest;
import edu.stanford.cs244b.sofajraft.proto.DbResponseOuterClass.DbResponse;
import edu.stanford.cs244b.sofajraft.proto.DatabaseUpdateRequest.DbUpdateRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;

@CrossOrigin(origins = "*")
@RestController
class WebController {

	@RequestMapping(value = "/sql", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public RestClientSqlResponse process(@RequestBody RestClientSqlRequest requestBody){
		System.out.println("Printing Incoming Request - " + requestBody.toString());
		
		RestClientSqlResponse clientResponse = new RestClientSqlResponse();

		/*
		* Basic Parsing of Incoming request
		* We should probable have a separate private function
		* to sanitize and parse request
		*/
		if (requestBody.getDbName() == null || requestBody.getSqlCommand() == null ||
				requestBody.getGroupId() == null) {
			clientResponse.setStatusCode(HttpStatus.BAD_REQUEST.toString());
			return clientResponse;
		}
		String sqlCommand = requestBody.getSqlCommand();
		String groupId = requestBody.getGroupId();
		String typeOfSqlCommand = sqlCommand.split(" ")[0].toUpperCase();

		if (typeOfSqlCommand.equals("SELECT")) {
			DbReadRequest dbReadRequest = DbReadRequest.newBuilder()
					.setDbName(requestBody.getDbName())
					.setSqlCommand(requestBody.getSqlCommand())
					.setConsistencyLevel(DbReadRequest.ConsistencyLevel.WEAK)
					.build();
			callRpcServer(groupId, dbReadRequest, clientResponse);
			clientResponse.setCommandType("READ");
		} else {
			DbUpdateRequest dbUpdateRequest = DbUpdateRequest.newBuilder()
					.setDbName(requestBody.getDbName())
					.setSqlCommand(requestBody.getSqlCommand())
					.build();
			callRpcServer(groupId, dbUpdateRequest, clientResponse);
			clientResponse.setCommandType("UPDATE");
		}
		return clientResponse;
	}

	@RequestMapping(value = "/members", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, params = {"groupId"})
	public RestClientAllMembersResponse listAllMembers(@RequestParam(value = "groupId") String groupId) {
		RestClientAllMembersResponse response = new RestClientAllMembersResponse();

		CliClientServiceImpl cliClientService = RpcClientFactory.getCliClientService(groupId);
		RpcClientFactory.refreshCurrentLeader(groupId);
		final PeerId leader = RpcClientFactory.getLeader(groupId);
		System.out.println("Leader is " + leader);

		List<PeerId> rpcResult = SqliteClusterMembership.getPeers(groupId, leader, cliClientService);
		List<RaftPeerInfo> peerInfos = new ArrayList<>();
		for (PeerId peerId : rpcResult) {
			RaftPeerInfo raftPeerInfo = new RaftPeerInfo();
			raftPeerInfo.setIp(peerId.getIp());
			raftPeerInfo.setPort(String.valueOf(peerId.getPort()));
			raftPeerInfo.setGroupId(groupId);

			peerInfos.add(raftPeerInfo);
		}
		response.setGroupId(groupId);
		response.setLeaderAddress(leader.toString());
		response.setStatusCode(HttpStatus.OK.toString());
		response.setPeerInfoList(peerInfos);
		return  response;
	}

	@RequestMapping(value = "/members/modify", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public RestClientAddRemoveMemberResponse addOrRemoveRaftMember(@RequestBody RestClientAddRemoveMemberRequest restClientAddRemoveMemberRequest) {
		RestClientAddRemoveMemberResponse restClientAddMemberResponse = new RestClientAddRemoveMemberResponse();
		if (restClientAddRemoveMemberRequest.getGroupId() == null ||
				restClientAddRemoveMemberRequest.getIp() == null ||
		        restClientAddRemoveMemberRequest.getPort() == null ||
				(!restClientAddRemoveMemberRequest.getAction().equals("ADD") &&
						!restClientAddRemoveMemberRequest.getAction().equals("REMOVE")) ) {
			// TODO - Add more screening here - IP format etc.
			restClientAddMemberResponse.setStatusCode(HttpStatus.BAD_REQUEST.toString());
			return restClientAddMemberResponse;
		}
		String groupId = restClientAddRemoveMemberRequest.getGroupId();
		CliClientServiceImpl cliClientService = RpcClientFactory.getCliClientService(groupId);
		RpcClientFactory.refreshCurrentLeader(groupId);
		PeerId leader = RpcClientFactory.getLeader(groupId);
		RaftPeerInfo raftPeerInfo = new RaftPeerInfo();
		if (restClientAddRemoveMemberRequest.getAction().equals("ADD")) {
			PeerId addedMember = SqliteClusterMembership.addNode(groupId, leader,
					cliClientService,
					restClientAddRemoveMemberRequest.getIp(),
					Integer.valueOf(restClientAddRemoveMemberRequest.getPort()));
			if (addedMember == null) {
				restClientAddMemberResponse.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
				restClientAddMemberResponse.setErrorMessage("Failed to add member to Raft Group");
				return restClientAddMemberResponse;
			}
			raftPeerInfo.setIp(addedMember.getIp());
			raftPeerInfo.setPort(String.valueOf(addedMember.getPort()));
		} else if (restClientAddRemoveMemberRequest.getAction().equals("REMOVE")) {
			PeerId deletedMember = SqliteClusterMembership.deleteNode(groupId, leader,
					cliClientService,
					restClientAddRemoveMemberRequest.getIp(),
					Integer.valueOf(restClientAddRemoveMemberRequest.getPort()));
			if (deletedMember == null) {
				restClientAddMemberResponse.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
				restClientAddMemberResponse.setErrorMessage("Failed to remove member from Raft Group");
				return restClientAddMemberResponse;
			}
			raftPeerInfo.setIp(deletedMember.getIp());
			raftPeerInfo.setPort(String.valueOf(deletedMember.getPort()));
		}

		raftPeerInfo.setGroupId(groupId);

		restClientAddMemberResponse.setStatusCode(HttpStatus.OK.toString());
		// Refresh leader before sending response
		RpcClientFactory.refreshCurrentLeader(groupId);
		leader = RpcClientFactory.getLeader(groupId);
		if(leader != null)
			restClientAddMemberResponse.setLeaderAddress(leader.toString());
		restClientAddMemberResponse.setRaftPeerInfo(raftPeerInfo);
		return restClientAddMemberResponse;
	}

	private void callRpcServer(String groupId, Object request, RestClientSqlResponse clientResponse ) {
		CliClientServiceImpl cliClientService = RpcClientFactory.getCliClientService(groupId);
		RpcClientFactory.refreshCurrentLeader(groupId);
		final PeerId leader = RpcClientFactory.getLeader(groupId);
		System.out.println("Leader is " + leader);
		final CountDownLatch latch = new CountDownLatch(1);

		try {
			if (request instanceof DbReadRequest) {
				cliClientService.getRpcClient()
						.invokeAsync(leader.getEndpoint(), (DbReadRequest) request,
								getCallBack(clientResponse, latch), 5000);
				latch.await();
				System.out.println("DB read request finished.");
			} else if (request instanceof DbUpdateRequest) {
				cliClientService.getRpcClient()
						.invokeAsync(leader.getEndpoint(), (DbUpdateRequest) request,
								getCallBack(clientResponse, latch), 5000);
				latch.await();
				System.out.println("DB Update request finished.");
			} else {
				System.out.println("Error: Request type not supported");
			}
		} catch (Exception ex) {
			System.out.println("Exception Occurred ");
			ex.printStackTrace();
			clientResponse.setErrorMessage(HttpStatus.SERVICE_UNAVAILABLE.toString());
		}
	}

	private InvokeCallback getCallBack(RestClientSqlResponse clientResponse, CountDownLatch latch){
		InvokeCallback callback = new InvokeCallback() {
			@Override
			public void complete(Object o, Throwable throwable) {
				DbResponse response = (DbResponse) o;
				if (response.getSuccess()) {
					clientResponse.setStatusCode(HttpStatus.OK.toString());
					clientResponse.setLeaderAddress(response.getRedirect());
					clientResponse.setResult(response.getResult());
				} else {
					clientResponse.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
					clientResponse.setErrorMessage(response.getErrorMsg());
				}
				latch.countDown();
			}

			@Override
			public Executor executor() {
				return null;
			}
		};
		return callback;
	}
}
