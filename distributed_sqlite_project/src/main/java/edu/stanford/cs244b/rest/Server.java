package edu.stanford.cs244b.rest;


import edu.stanford.cs244b.configuration.ConfigurationFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.boot.SpringApplication;


@ComponentScan
@Configuration
@SpringBootApplication
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
public class Server 
{
    public static void main( String[] args )
    {
        System.out.println( "Starting Distributed Sqlite Server node" );
        System.out.println("Sample Usage from a different terminal : curl --data '{\"id\":\"234\",\"request\":\"xyz\"}' http://localhost:8080/sql  --header \"Content-Type:application/json\"");
        SpringApplication.run(Server.class, args);
    }
}
