package edu.stanford.cs244b.benchmark;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;

import com.alipay.sofa.jraft.entity.PeerId;
import com.alipay.sofa.jraft.error.RemotingException;
import com.alipay.sofa.jraft.rpc.impl.cli.CliClientServiceImpl;

import edu.stanford.cs244b.rest.RpcClientFactory;
import edu.stanford.cs244b.sofajraft.proto.DatabaseReadRequest;
import edu.stanford.cs244b.sofajraft.proto.DatabaseUpdateRequest;
import edu.stanford.cs244b.sofajraft.proto.DbResponseOuterClass;

/**
 * Class for running performance tests against dslqite
 *
 * Test cases:
 *     - Sequential writes.
 *     - Random writes.
 *     - Sequential reads.
 *     - Random reads.
 */
public class BenchmarkTest {
    final static String testDbName = "benchmark";
    final static String createTableSql = "CREATE TABLE IF NOT EXISTS benchmark (\n"
                            + "	id integer PRIMARY KEY,\n"
                            + "	name text NOT NULL,\n"
                            + "	number real\n"
                            + ");";

    final static String insertSequentialSql = "INSERT INTO benchmark(name,number) VALUES(\"benchmark\", %d);";

    // should probably change to more typical queries. example from historical site: https://www.sqlite.org/speed.html
    final static String updateRandomSql = "UPDATE benchmark SET number=%d WHERE benchmark.id IN (SELECT id FROM benchmark ORDER BY RANDOM() LIMIT 1);";
    final static String readSequentialSql = "SELECT * FROM benchmark WHERE id=%d;";
    final static String readRandomSql = "SELECT * FROM benchmark WHERE id=%d;";

    enum Operation {
        READ,
        INSERT,
        UPDATE
    }

    enum Order {
        SEQUENTIAL,
        RANDOM
    }

    public static void main(final String[] args) throws Exception {
        if (args.length != 4) {
            System.out.println("Usage : java edu.stanford.cs244b.benchmark.BenchmarkTest {groupId} {operation} {order} {iteration}");
            System.out.println("Example: edu.stanford.cs244b.benchmark.BenchmarkTest dsqlite INSERT SEQUENTIAL 200");
            System.exit(1);
        }
        final String groupId = args[0];
        final Operation operation = Operation.valueOf(args[1]);
        final Order order = Order.valueOf(args[2]);
        final int iters = Integer.parseInt(args[3]);

        final CliClientServiceImpl cliClientService = RpcClientFactory.getCliClientService(groupId);
        RpcClientFactory.refreshCurrentLeader(groupId);
        final PeerId leader = RpcClientFactory.getLeader(groupId);
        System.out.println("Leader is " + leader);
        createTestTable(leader, cliClientService, createTableSql);

        if (operation == Operation.INSERT && order == Order.SEQUENTIAL) {
            insertSequentialTest(leader, cliClientService, iters , insertSequentialSql);
        } else if (operation == Operation.UPDATE && order == Order.RANDOM) {
            updateRandomRowTest(leader, cliClientService, iters, updateRandomSql);
        } else if (operation == Operation.READ && order == Order.SEQUENTIAL) {
            readSequentialTest(leader, cliClientService, iters, readSequentialSql);
        } else if (operation == Operation.READ && order == Order.RANDOM) {
            readRandomTest(leader, cliClientService, iters, readRandomSql);
        } else {
            throw new UnsupportedOperationException();
        }
    }

    private static void createTestTable(PeerId leader, CliClientServiceImpl cliClientService, String sql)
            throws InterruptedException, RemotingException {
        DatabaseUpdateRequest.DbUpdateRequest request = DatabaseUpdateRequest.DbUpdateRequest.newBuilder().setDbName(testDbName).setSqlCommand(sql).build();
        cliClientService.getRpcClient().invokeSync(leader.getEndpoint(), request, 5000);
        System.out.println("Created test table: " + testDbName);
    }

    private static void insertSequentialTest(PeerId leader, CliClientServiceImpl cliClientService, int iters, String sql)
            throws InterruptedException, RemotingException {
        List<DatabaseUpdateRequest.DbUpdateRequest> requests = new ArrayList<>();
        for (int i = 0; i < iters; i++) {
            requests.add(DatabaseUpdateRequest.DbUpdateRequest.newBuilder()
                                 .setDbName(testDbName)
                                 .setSqlCommand(String.format(sql, new Random().nextInt(iters)))
                                 .build());
        }

        long testStartTime = System.currentTimeMillis();
        System.out.println("================================================================");
        System.out.println("DB sequential insert request started at: " + testStartTime);
        for (DatabaseUpdateRequest.DbUpdateRequest request : requests) {
            cliClientService.getRpcClient().invokeSync(leader.getEndpoint(), request, 5000);
        }
        long testEndTime = System.currentTimeMillis();
        System.out.println(String.format("DB %d sequential inserts test finished in %.2f seconds", iters, (testEndTime - testStartTime) / 1000.0));
    }

    private static void updateRandomRowTest(PeerId leader, CliClientServiceImpl cliClientService, int iters, String sql)
            throws InterruptedException, RemotingException {
        List<DatabaseUpdateRequest.DbUpdateRequest> requests = new ArrayList<>();
        for (int i = 0; i < iters; i++) {
            requests.add(DatabaseUpdateRequest.DbUpdateRequest.newBuilder()
                                 .setDbName(testDbName)
                                 .setSqlCommand(String.format(sql, new Random().nextInt(iters)))
                                 .build());
        }

        long testStartTime = System.currentTimeMillis();
        System.out.println("================================================================");
        System.out.println("DB random update request started at: " + testStartTime);
        for (DatabaseUpdateRequest.DbUpdateRequest request : requests) {
            cliClientService.getRpcClient().invokeSync(leader.getEndpoint(), request, 5000);
        }
        long testEndTime = System.currentTimeMillis();
        System.out.println(String.format("DB %d random update test finished in %.2f seconds", iters, (testEndTime - testStartTime) / 1000.0));
    }

    private static void readSequentialTest(PeerId leader, CliClientServiceImpl cliClientService, int iters, String sql)
            throws InterruptedException, RemotingException {
        List<DatabaseReadRequest.DbReadRequest> requests = new ArrayList<>();
        DatabaseReadRequest.DbReadRequest dbReadRequest = DatabaseReadRequest.DbReadRequest.newBuilder()
                .setSqlCommand("SELECT COUNT(*) FROM benchmark;")
                .setConsistencyLevel(DatabaseReadRequest.DbReadRequest.ConsistencyLevel.STRONG)
                .setDbName(testDbName)
                .build();
        DbResponseOuterClass.DbResponse response = (DbResponseOuterClass.DbResponse) cliClientService.getRpcClient().invokeSync(leader.getEndpoint(), dbReadRequest, 5000);
        JSONObject json = (JSONObject) (new JSONArray(response.getResult()).get(0));
        int rowCount = json.getInt("COUNT(*)");
        for (int i = 0; i < iters; i++) {
            requests.add(DatabaseReadRequest.DbReadRequest.newBuilder()
                                 .setDbName(testDbName)
                                 .setSqlCommand(String.format(sql, i % rowCount))
                                 .build());
        }

        long testStartTime = System.currentTimeMillis();
        System.out.println("DB sequential read request started at: " + testStartTime);
        for (DatabaseReadRequest.DbReadRequest request : requests) {
            cliClientService.getRpcClient().invokeSync(leader.getEndpoint(), request, 5000);
        }
        long testEndTime = System.currentTimeMillis();
        System.out.println(String.format("DB %d sequential read test finished in %.2f seconds", iters, (testEndTime - testStartTime) / 1000.0));
    }

    private static void readRandomTest(PeerId leader, CliClientServiceImpl cliClientService, int iters, String sql)
            throws InterruptedException, RemotingException {
        List<DatabaseReadRequest.DbReadRequest> requests = new ArrayList<>();
        DatabaseReadRequest.DbReadRequest dbReadRequest = DatabaseReadRequest.DbReadRequest.newBuilder()
                .setSqlCommand("SELECT COUNT(*) FROM benchmark;")
                .setConsistencyLevel(DatabaseReadRequest.DbReadRequest.ConsistencyLevel.STRONG)
                .setDbName(testDbName)
                .build();
        DbResponseOuterClass.DbResponse response = (DbResponseOuterClass.DbResponse) cliClientService.getRpcClient().invokeSync(leader.getEndpoint(), dbReadRequest, 5000);
        JSONObject json = (JSONObject) (new JSONArray(response.getResult()).get(0));
        int rowCount = json.getInt("COUNT(*)");
        for (int i = 0; i < iters; i++) {
            requests.add(DatabaseReadRequest.DbReadRequest.newBuilder()
                                 .setDbName(testDbName)
                                 .setSqlCommand(String.format(sql, new Random().nextInt(rowCount)))
                                 .build());
        }

        long testStartTime = System.currentTimeMillis();
        System.out.println("DB sequential read request started at: " + testStartTime);
        for (DatabaseReadRequest.DbReadRequest request : requests) {
            cliClientService.getRpcClient().invokeSync(leader.getEndpoint(), request, 5000);
        }
        long testEndTime = System.currentTimeMillis();
        System.out.println(String.format("DB %d sequential read test finished in %.2f seconds", iters, (testEndTime - testStartTime) / 1000.0));
    }

}
