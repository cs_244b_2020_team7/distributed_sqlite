package edu.stanford.cs244b.twopc;

import java.util.Set;
import edu.stanford.cs244b.twopc.Node;
import edu.stanford.cs244b.twopc.proto.*;
import edu.stanford.cs244b.twopc.proto.DecisionMessage.*;
import edu.stanford.cs244b.twopc.proto.VoteResultOuterClass.*;
import edu.stanford.cs244b.twopc.proto.DistributedTransactionOuterClass.*;
import java.util.concurrent.CountDownLatch;

/*disclamer this code is modified based on github open sourced 2pc implementation */
public interface Coordinator extends Node {
    // Get List of Participants.
    Set<String> participants();
    // Send Vote Request to all participants.
    DistributedTransaction sendVoteRequest(DistributedTransaction transaction, CountDownLatch latch);
    // Make decision for all participants.
    Decision.DecisionType  makeDecision(DistributedTransaction transaction );
    // Commit Or Abort.
    void notifyDecision(DistributedTransaction transaction, Decision.DecisionType decision, CountDownLatch latch);
    // Recover if crash.
    void recover();
}