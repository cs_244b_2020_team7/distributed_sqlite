package edu.stanford.cs244b.twopc;

import edu.stanford.cs244b.twopc.proto.*;
import edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.*;
import edu.stanford.cs244b.twopc.proto.DistributedTransactionOuterClass.*;

/*disclamer this code is modified based on github open sourced 2pc implementation */
public interface LogKeeper {
    void recordMsg(DistributedTransaction transaction, TransactionStatus type);
}