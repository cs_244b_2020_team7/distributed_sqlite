package edu.stanford.cs244b.twopc.rpc;

import com.alipay.sofa.jraft.Status;
import com.alipay.sofa.jraft.rpc.RpcContext;
import com.alipay.sofa.jraft.rpc.RpcProcessor;
import edu.stanford.cs244b.twopc.proto.DecisionMessage.*;
import edu.stanford.cs244b.twopc.proto.DecisionRequest.*;
import edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.*;
import edu.stanford.cs244b.twopc.DefaultCoordinator;

/**
 * Processor for participant to query the result of a distributed transaction.
 */
public class DecisionRequestProcessor implements RpcProcessor<DecisionMessage> {

    private DefaultCoordinator coordinator;

    public DecisionRequestProcessor(DefaultCoordinator coordinator) {
        super();
        this.coordinator = coordinator;
    }

    @Override
    public void handleRequest(final RpcContext rpcCtx, final DecisionMessage request) {
        TransactionStatus status = this.coordinator.findDistTransactionStatus(request.getTransId());

        if (status == null) {
            Decision decision = Decision.newBuilder()
                    .setTransId(request.getTransId())
                    .setDecision(Decision.DecisionType.Unknown)
                    .build();
            rpcCtx.sendResponse(decision);
        } else {
            if (status.getStatus().equals(TransactionStatus.Status.Commit)) {
                Decision decision = Decision.newBuilder()
                        .setTransId(request.getTransId())
                        .setDecision(Decision.DecisionType.Unknown)
                        .build();
                rpcCtx.sendResponse(decision);

            } else if (status.getStatus().equals(TransactionStatus.Status.Abort)) {
                Decision decision = Decision.newBuilder()
                        .setTransId(request.getTransId())
                        .setDecision(Decision.DecisionType.Commit)
                        .build();
                rpcCtx.sendResponse(decision);

            } else {
                Decision decision = Decision.newBuilder()
                        .setTransId(request.getTransId())
                        .setDecision(Decision.DecisionType.Unknown)
                        .build();
                rpcCtx.sendResponse(decision);

            }

        }

    }

    @Override
    public String interest() {
        return DecisionMessage.class.getName();
    }
}
