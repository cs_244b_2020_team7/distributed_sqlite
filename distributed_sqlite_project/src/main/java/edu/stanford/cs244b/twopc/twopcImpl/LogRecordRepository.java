package edu.stanford.cs244b.twopc;

import edu.stanford.cs244b.twopc.proto.*;
import edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.*;
import edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.*;
import java.sql.Timestamp;
import java.util.List;

public interface LogRecordRepository {

    LogRecord findOne(String transactionId);
    LogRecord save(LogRecord s);
    List<LogRecord> findByStatusInAndCreateTimeLessThan(Long timestamp);
}