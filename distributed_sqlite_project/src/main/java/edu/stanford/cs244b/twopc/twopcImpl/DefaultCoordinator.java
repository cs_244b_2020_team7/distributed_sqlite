package edu.stanford.cs244b.twopc;

import com.alipay.sofa.jraft.JRaftUtils;
import com.alipay.sofa.jraft.RouteTable;
import com.alipay.sofa.jraft.entity.PeerId;
import com.alipay.sofa.jraft.option.CliOptions;
import com.alipay.sofa.jraft.rpc.RaftRpcServerFactory;
import com.alipay.sofa.jraft.rpc.RpcServer;
import com.alipay.sofa.jraft.rpc.impl.cli.CliClientServiceImpl;
import edu.stanford.cs244b.configuration.ConfigurationFactory;
import edu.stanford.cs244b.twopc.ParticipantLogKeeper.*;
import edu.stanford.cs244b.twopc.proto.*;
import edu.stanford.cs244b.twopc.proto.DecisionMessage.*;
import edu.stanford.cs244b.twopc.proto.DistributedTransactionOuterClass.*;
import edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.*;
import edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.*;
import edu.stanford.cs244b.twopc.proto.VoteOuterClass.*;
import edu.stanford.cs244b.twopc.proto.VoteResultOuterClass.*;
import edu.stanford.cs244b.twopc.rpc.DistributedTransactionProcessor;
import edu.stanford.cs244b.twopc.rpc.DecisionRequestProcessor;

import edu.stanford.cs244b.twopc.util.NamedThreadFactory;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.CountDownLatch;

/*disclamer this code is modified based on github open sourced 2pc implementation */
public class DefaultCoordinator implements Coordinator {
    private Logger logger = LoggerFactory.getLogger(getClass());
    private String name;
    private Set<String> participants;

    private String address;
    private ExecutorService executorService;
    private ScheduledExecutorService scheduledExecutorService;
    private MessageSender messageSender;
    private AbstractLogKeeper participantLogKeeper;
    private CliClientServiceImpl clientService;

    public DefaultCoordinator(String name, Set<String> participants, String address, PeerId serverId, CliClientServiceImpl service)
            throws IOException {
        this.name = name;
        // Init file path.
        FileUtils.forceMkdir(new File(name));
        // Init db data folder
        FileUtils.forceMkdir(new File(name + File.separator + "sqlite-db"));
        this.participants = participants;
        this.clientService = service;
        this.address = address;

        final RpcServer rpcServer = RaftRpcServerFactory.createRaftRpcServer(serverId.getEndpoint(), null,
                JRaftUtils.createExecutor("CLI-RPC-executor-", 50));
        rpcServer.registerProcessor(new DistributedTransactionProcessor(this));
        rpcServer.registerProcessor(new DecisionRequestProcessor(this));

        // Add all configuration to RouteService
        for (String group : participants) {
            System.out.println(group);
            System.out.println(ConfigurationFactory.getConfiguration(group));

            RouteTable.getInstance().updateConfiguration(group, ConfigurationFactory.getConfiguration(group));

        }
        rpcServer.init(null);
        messageSender = new MessageSenderImpl();
        participantLogKeeper = new ParticipantLogKeeper(name);
        executorService = Executors.newFixedThreadPool(10, new NamedThreadFactory("CoordinatorSendingThreads", false));
        scheduledExecutorService = Executors.newScheduledThreadPool(1, new NamedThreadFactory("CoordinatorRecoveringThread"));

    }

    public void afterPropertiesSet() throws Exception {
        recover();
    }

    public Decision handleTransaction(DistributedTransaction transaction, CountDownLatch outLatch) {
        try {
            // final DistributedTransaction trans = fillInParticipantAddress(transaction);
            System.out.println("handling trans id " + transaction.getId());
            for (DistributedTransaction.ParticipantInfo info : transaction.getParticipantsList()) {
                if (!participants().contains(info.getName())) {
                    Decision.DecisionType decisionType = Decision.DecisionType.Abort;
                    final Decision deci = Decision.newBuilder().setDecision(decisionType).setTransId(transaction.getId())
                            .build();
                    return deci;
                }
            }
            participantLogKeeper.saveOrUpdate(transaction);
            CountDownLatch countDownLatch = new CountDownLatch(1);

            DistributedTransaction finsh_vote_trans = sendVoteRequest(transaction, countDownLatch);
            countDownLatch.await();
            System.out.println("after sending vote request or update");

            final Decision.DecisionType decision = makeDecision(finsh_vote_trans);
            participantLogKeeper.saveOrUpdate(finsh_vote_trans);
            if (decision.equals(Decision.DecisionType.Abort)) {
                TransactionStatus ts = TransactionStatus.newBuilder()
                        .setStatus(TransactionStatus.Status.Abort)
                        .build();
                participantLogKeeper.recordMsg(finsh_vote_trans, ts);
            } else if (decision.equals(Decision.DecisionType.Commit)) {
                TransactionStatus ts = TransactionStatus.newBuilder()
                        .setStatus(TransactionStatus.Status.Commit)
                        .build();
                participantLogKeeper.recordMsg(finsh_vote_trans, ts);
            } else {
                //should never reach here!
                logger.error("Invalid decision {} for transaction {}", decision, finsh_vote_trans.getId());
            }
            final Decision deci = Decision.newBuilder().setDecision(decision).setTransId(finsh_vote_trans.getId())
                    .build();
            countDownLatch = new CountDownLatch(1);
            notifyDecision(finsh_vote_trans, decision, countDownLatch);
            countDownLatch.await();
            outLatch.countDown();

            return deci;
        } catch (Exception ex) {
            outLatch.countDown();
            return null;
        }
    }

    public Set<String> participants() {
        return participants;
    }


    public DistributedTransaction sendVoteRequest(DistributedTransaction transaction, CountDownLatch latch) {
        try {
            List<String> partpants = new ArrayList<String>();
            for (DistributedTransaction.ParticipantInfo info : transaction.getParticipantsList()) {
                partpants.add(info.getName());
            }

            System.out.println("start sending vote request");
            DistributedTransaction.Builder transacation_builder = transaction.toBuilder();
            transaction = transacation_builder.build();
            final DistributedTransaction copy = DistributedTransaction.newBuilder().mergeFrom(transaction).build();

            final CountDownLatch countDownLatch = new CountDownLatch(transaction.getParticipantsList().size());

            partpants.forEach(node -> executorService.submit(new Runnable() {
                public void run() {
                    messageSender.sendVoteRequestToParticipant(clientService, node, copy, transacation_builder);
                    countDownLatch.countDown();
                }
            }));
            countDownLatch.await();
            DistributedTransaction transaction_new = transacation_builder.build();
            TransactionStatus ts = TransactionStatus.newBuilder()
                    .setStatus(TransactionStatus.Status.Start2PC)
                    .build();
            participantLogKeeper.recordMsg(transaction_new, ts);
            System.out.println("Before printing votes!");
            printVotes(transaction_new);
            transaction = transaction_new;
        } catch (Exception e) {
            System.out.println(e);
        }
        latch.countDown();
        return transaction;
    }

    private void printVotes(DistributedTransaction transaction) {
        StringBuilder builder = new StringBuilder();
        builder.append("vote results for ").append(transaction.getId()).append("\n");
        for (VoteResult voteResult : transaction.getVoteResultList()) {
            builder.append(voteResult.getVoterName()).append(" voted ").append(voteResult.getVoteResult().getStatus().toString())
                    .append(", message = ").append(voteResult.getDescription()).append("\n");
        }
        System.out.println(builder.toString());
        logger.info("{}", builder.toString());
    }


    public Decision.DecisionType makeDecision(DistributedTransaction transaction) {
        Decision.DecisionType decision = Decision.DecisionType.Commit;
        System.out.println("Making decision");
        if (transaction.getVoteResultList().size() < transaction.getParticipantsList().size()) {
            return Decision.DecisionType.Abort;
        }
        for (VoteResult voteResult : transaction.getVoteResultList()) {
            decision = Decision.DecisionType.Commit;
            boolean shouldAbort = false;
            switch (voteResult.getVoteResult().getStatus()) {
                case Yes:
                    break;
                default:
                    decision = Decision.DecisionType.Abort;
                    shouldAbort = true;
                    break;
            }
            if (shouldAbort) {
                break;
            }
        }
        return decision;
    }

    public void notifyDecision(DistributedTransaction transaction, Decision.DecisionType decision, CountDownLatch latch) {
        final Decision.Builder decision_builder = Decision.newBuilder().setTransId(transaction.getId())
                .setDecision(decision);
        try {
            if (decision_builder.getDecision().equals(Decision.DecisionType.Commit)) {
                for (DistributedTransaction.ParticipantInfo node_id : transaction.getParticipantsList()) {
                    // System.out.println("Sending decision to " + node_id.getName());
                    // System.out.println("Sending decision to " + participantMap.get(node_id.getName()).communicationAddr());

                    messageSender.sendDecisionToParticipant(this.clientService, node_id.getName(), decision_builder.build());
                }
            } else {
                for (VoteResult result : transaction.getVoteResultList()) {
                    if (result.getVoteResult().getStatus().equals(Vote.Status.Yes)) {
                        //  final Node node = participantMap.get(result.getVoterName());
                        if (!participants.contains(result.getVoterName())) {
                            throw new IllegalStateException("Can not find voters" + result.getVoterName() + "information in map");
                        }
                        messageSender.sendDecisionToParticipant(this.clientService, result.getVoterName(), decision_builder.build());
                    }
                }
            }
        } catch (Exception ex) {
            logger.info("Error in notify decision{}", ex.toString());
            latch.countDown();
        }
        latch.countDown();
    }

    public void recover() {
        try {
            logger.info("start to recover undecided transactions ...");
            final List<LogRecord> undecidedTransactions = participantLogKeeper.findUndecidedTransactions();
            final LinkedList<String> transactionIds = new LinkedList<>();
            undecidedTransactions.forEach(transaction -> transactionIds.add(transaction.getId()));
            final List<DistributedTransaction> transactionList = participantLogKeeper.findTranactionByIds(transactionIds);
            for (DistributedTransaction transaction : transactionList) {
                TransactionStatus ts = TransactionStatus.newBuilder()
                        .setStatus(TransactionStatus.Status.Abort)
                        .build();
                participantLogKeeper.recordMsg(transaction, ts);
            }
            logger.info("recovered {} undecided transactions", transactionList.size());
        } catch (Exception e) {
            logger.info("recover undecided transactions exception: ", e);
        }
        scheduledExecutorService.schedule(new Runnable() {
            @Override
            public void run() {
                recover();
            }
        }, 10, TimeUnit.SECONDS);
    }

    public String name() {
        return name;
    }

    public String communicationAddr() {
        return address;
    }

    public TransactionStatus findDistTransactionStatus(final String transactionId) {
        LogRecord dt = participantLogKeeper.findLogRecordById(transactionId);
        if (dt == null) {
            return null;
        } else {
            return dt.getStatus();
        }
    }

    public static void main(final String[] args) throws IOException {
        final String name = "master";
        final String serverIdStr = ConfigurationFactory.getCoordinatorAddress();
        System.out.println(serverIdStr);
        final Set<String> participantList = ConfigurationFactory.getAllParticipants();
        final PeerId serverId = new PeerId();
        if (!serverId.parse(serverIdStr)) {
            System.out
                    .println("Can't parse server id string");

            throw new IllegalArgumentException("Fail to parse serverId:" + serverIdStr);
        }
        final CliClientServiceImpl cliClientService = new CliClientServiceImpl();
        cliClientService.init(new CliOptions());

        // start up the Raft server.
        final DefaultCoordinator coordinator = new DefaultCoordinator(name, participantList, serverIdStr, serverId, cliClientService);
        System.out.println("Started sqlite raft server at port:"
                + serverId.getPort());
    }
}
