package edu.stanford.cs244b.twopc;

import java.net.URL;

/*disclamer this code is modified based on github open sourced 2pc implementation */
public interface Node {
    String name();
    String communicationAddr();
}