package edu.stanford.cs244b.twopc;

import edu.stanford.cs244b.twopc.*;
import edu.stanford.cs244b.twopc.proto.DistributedTransactionOuterClass.*;
import edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.*;

import java.util.List;

public class ParticipantLogKeeper extends AbstractLogKeeper {
    private LogRecordService logRecordService;
    private TransactionService transactionService;
    private String name;
    public ParticipantLogKeeper(String serverFilePath){
        logRecordService = new LogRecordService(serverFilePath);
        transactionService = new TransactionService(serverFilePath);
    }
    @Override
    public void saveOrUpdate(LogRecord logRecord) {
        logRecordService.saveOrUpdate(logRecord);
    }

    @Override
    public void saveOrUpdate(DistributedTransaction distributedTransaction) {
        transactionService.saveOrUpdate(distributedTransaction);
    }

    @Override
    public List<LogRecord> findUndecidedTransactions() {
        return logRecordService.findUndecidedTransactions();
    }

    @Override
    public DistributedTransaction findTranactionById(String Id) {
        return transactionService.findTranactionById(Id);
    }

    @Override
    public LogRecord findLogRecordById(String Id) {
        return logRecordService.findOne(Id);
    }

    @Override
    public List<DistributedTransaction> findTranactionByIds(List<String> Id) {
        return transactionService.findTranactionByIds(Id);
    }
}