package edu.stanford.cs244b.twopc;

import com.alipay.sofa.jraft.option.CliOptions;
import com.alipay.sofa.jraft.rpc.impl.cli.CliClientServiceImpl;
import edu.stanford.cs244b.twopc.proto.*;
import edu.stanford.cs244b.twopc.proto.DecisionMessage.*;
import edu.stanford.cs244b.twopc.proto.DecisionRequest.*;
import edu.stanford.cs244b.twopc.proto.DistributedTransactionOuterClass.*;
import edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.*;
import edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.*;
import edu.stanford.cs244b.twopc.proto.VoteResultOuterClass.*;
import edu.stanford.cs244b.twopc.util.*;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/*disclamer this code is modified based on github open sourced 2pc implementation */
public abstract class AbstractParticipant implements Participant {
    private Logger logger = LoggerFactory.getLogger(getClass());
    private String name;
    private String address;
    private ScheduledExecutorService scheduledExecutorService;
    private Node transactionManagerNode;
    private MessageSender messageSender = new MessageSenderImpl();
    private CliClientServiceImpl clientService;
    private AbstractLogKeeper participantLogKeeper;

    public AbstractParticipant(String name, String address, String coordinatorAddress) throws IOException {
        // Init file path.
        FileUtils.forceMkdir(new File(name));
        // Init db data folder
        FileUtils.forceMkdir(new File(name + File.separator + "sqlite-db"));
        this.name = name;
        this.address = address;
        this.transactionManagerNode = new NodeImpl("master", coordinatorAddress);
        clientService = new CliClientServiceImpl();
        clientService.init(new CliOptions());
        participantLogKeeper = new ParticipantLogKeeper(name);
        scheduledExecutorService = Executors.newScheduledThreadPool(1, new NamedThreadFactory("ParticipantRecoverThread"));
    }

    public void afterPropertiesSet() throws Exception {
         recover();
    }

    public void getTransactionManager() throws Exception {
        //  recover();
    }

    public AbstractLogKeeper getParticipantLogKeeper() {
        return participantLogKeeper;
    }

    public void setParticipantLogKeeper(AbstractLogKeeper participantLogKeeper) {
        this.participantLogKeeper = participantLogKeeper;
    }

    @Override
    public abstract void commit(String transaction_id);

    @Override
    public abstract void abort(String transaction_id);

    @Override
    public void recover() {
        try {
            logger.info("start to recover unfinished records...");
            final List<LogRecord> undecidedLogRecords = participantLogKeeper.findUndecidedTransactions();
            final LinkedList<String> uncertainIds = new LinkedList<>();
            for (LogRecord logRecord : undecidedLogRecords) {
                if (logRecord.getStatus().equals(TransactionStatus.Status.No)) {
                    TransactionStatus transactionStatus = TransactionStatus.newBuilder().setStatus(TransactionStatus.Status.Abort).build();
                    LogRecord.Builder lg = LogRecord.newBuilder().mergeFrom(logRecord).setStatus(transactionStatus);
                    participantLogKeeper.saveOrUpdate(lg.build());
                } else {
                    uncertainIds.add(logRecord.getId());
                }
            }

            if (!uncertainIds.isEmpty()) {
                final List<DistributedTransaction> transactionList = participantLogKeeper.findTranactionByIds(uncertainIds);
                for (DistributedTransaction transaction : transactionList) {
                    final Decision decision = resolveDecisionByCTP(clientService, transaction);
                    switch (decision.getDecision()) {
                        case Abort:
                            abort(transaction.getId());
                            break;
                        case Commit:
                            commit(transaction.getId());
                            break;
                        default:
                            break;
                    }
                }
                logger.info("We recovered {} amount of tasks", uncertainIds.size());
            }
        } catch (Exception e) {
            logger.error("Something goes wrong in tasks ", e);
        }
        scheduledExecutorService.schedule(new Runnable() {
            @Override
            public void run() {
                recover();
            }
        }, 20, TimeUnit.SECONDS);
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public Decision resolveDecisionByCTP(CliClientServiceImpl client, DistributedTransaction transaction) {
        final List<DistributedTransaction.ParticipantInfo> participants = transaction.getParticipantsList();
        DecisionRequest.DecisionMessage.Builder query_message_builder = DecisionRequest.DecisionMessage.newBuilder();
        final DecisionRequest.DecisionMessage decisionQueryMessage = query_message_builder.setTransId(transaction.getId()).build();
        try {
            Decision.Builder decision = Decision.newBuilder()
                    .setDecision(Decision.DecisionType.Abort);
            messageSender.sendDecisionQuery2Others(client, transactionManagerNode, decisionQueryMessage, decision);
            return decision.build();
        } catch (Exception ex) {
            logger.error("Something goes wrong in  getting decision tasks ", ex);
        }
        Decision decision = Decision.newBuilder().setDecision(Decision.DecisionType.Abort).build();
        return decision;
    }


    @Override
    public String communicationAddr() {
        return address;
    }
}