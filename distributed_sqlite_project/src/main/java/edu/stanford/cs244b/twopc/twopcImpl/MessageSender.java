package edu.stanford.cs244b.twopc;

import edu.stanford.cs244b.twopc.proto.*;
import edu.stanford.cs244b.twopc.proto.VoteResultOuterClass.*;
import edu.stanford.cs244b.twopc.proto.DistributedTransactionOuterClass.*;
import edu.stanford.cs244b.twopc.proto.DecisionMessage.*;
import edu.stanford.cs244b.twopc.proto.DecisionRequest.*;

import com.alipay.sofa.jraft.rpc.CliRequests;
import com.alipay.sofa.jraft.rpc.impl.cli.CliClientServiceImpl;

/*disclamer this code is modified based on github open sourced 2pc implementation */
public interface MessageSender {

    void sendVoteRequestToParticipant(CliClientServiceImpl cliClientService, String group,
                                      DistributedTransaction request, DistributedTransaction.Builder builder);

    // void sendDecisionToParticipant(final Node node, Decision decision_type);
    void sendDecisionToParticipant(CliClientServiceImpl cliClientService, String group,
                                   Decision message);

    public void sendDecisionQuery2Others(CliClientServiceImpl cliClientService,
                                         final Node node, DecisionRequest.DecisionMessage message,
                                         Decision.Builder decision);
}