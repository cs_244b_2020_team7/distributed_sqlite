package edu.stanford.cs244b.twopc;

import edu.stanford.cs244b.twopc.proto.*;
import edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.*;
import edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/*disclamer this code is modified based on github open sourced 2pc implementation */
public class LogRecordService {
    private LogRecordRepository logRecordRepository;

    public LogRecordService(String serverFilePath){
        logRecordRepository = new ParticipantLogRecordRepository(serverFilePath);
    }

    public void saveOrUpdate(LogRecord logRecord) {
        LogRecord one = logRecordRepository.findOne(logRecord.getId());
        LogRecord.Builder builder = LogRecord.newBuilder();
        if (one == null) {
            builder.setId(logRecord.getId());
            builder.setStatus(logRecord.getStatus());
            builder.setCreateTime((new Date().getTime()));
            builder.setLastModifyTime(new Date().getTime());
            one = builder.build();
        } else {
            builder.mergeFrom(logRecord);
            builder.setStatus(logRecord.getStatus());
            builder.setLastModifyTime(new Date().getTime());
        }
        logRecordRepository.save(builder.build());
    }

    public LogRecord findOne(String transactionId) {
        final LogRecord one = logRecordRepository.findOne(transactionId);
        return one;
    }

    public List<LogRecord> findUndecidedTransactions() {
        final Long timestamp = System.currentTimeMillis() - 5 * 1000;
        final List<LogRecord> logRecordEntities = logRecordRepository.findByStatusInAndCreateTimeLessThan(timestamp);
        List<LogRecord> records = new ArrayList<LogRecord>();
        for (LogRecord lg : records){
            if (lg.getStatus().getStatus().equals(TransactionStatus.Status.Start2PC)){
                records.add(lg);
            }
        }
        return records;
    }
}