# Distributed Sqlite 2pc implementation 
This 2pc implemention was implemented by referencing https://github.com/segeon/2pc/ project.
I mainly reference the structure and interface. The internal implementation has been changed
greatly to suit the need of project of distributed Sqlite. 

In the github repo, the original user
didn't put any license in the repo. I have full respects to the original author of https://github.com/segeon/2pc/.
