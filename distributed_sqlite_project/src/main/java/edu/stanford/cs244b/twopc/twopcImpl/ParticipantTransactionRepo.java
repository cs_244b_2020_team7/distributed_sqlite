package edu.stanford.cs244b.twopc;

import static edu.stanford.cs244b.sofajraft.service.SqliteRaftServer.DB_FOLDER_NAME;

import com.google.protobuf.Struct.Builder;
import com.google.protobuf.util.JsonFormat;
import edu.stanford.cs244b.sofajraft.jdbc.*;
import edu.stanford.cs244b.twopc.proto.*;
import edu.stanford.cs244b.twopc.proto.DistributedTransactionOuterClass.*;
import edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.*;
import edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.*;
import java.io.File;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ParticipantTransactionRepo implements TransactionRepository {
    private String databaseName;
    private String dbDataFolder;

    public final static String DB_SUFFIX = ".db";
    private final static ResultSetJsonExtractor resultSetJsonExtractor = new ResultSetJsonExtractor();
    private static final String CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS TRANSACTIONS (" +
                    "ID TEXT PRIMARY KEY, " +
                    "TS text NOT NULL);";
    SqliteJDBC sqliteJDBC;

    public ParticipantTransactionRepo(String serverFilePath) {
        this.dbDataFolder = String.format("jdbc:sqlite:%s", serverFilePath + File.separator + DB_FOLDER_NAME);
        this.databaseName = "TRANSACTION";
        sqliteJDBC = new SqliteJDBCImpl(serverFilePath);
        int i = sqliteJDBC.executeUpdate(this.databaseName,
                CREATE_TABLE);
        if (i != 1) {
            System.out.println("Creating transactions");
        }

    }

    @Override
    public DistributedTransaction findOne(String s) {
        DistributedTransaction.Builder trans = DistributedTransaction.newBuilder();
        Connection connection = null;
        boolean first = false;
        try {
            // create a database connection
            connection = DriverManager.getConnection(this.dbDataFolder + File.separator + this.databaseName + DB_SUFFIX);
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.
            System.out.println("Select * FROM TRANSACTIONS Where ID = '" + s + "';");
            ResultSet result = statement.executeQuery("Select * FROM TRANSACTIONS Where ID = '" + s + "';");
            while (result.next()) {
                if (first) {
                    break;
                }
                String id = result.getString("ID");
                String TS = result.getString("TS");
                JsonFormat.parser().merge(TS, trans);
                System.out.println(TS);
                first = true;
            }
        } catch (Exception e) {
            System.out.print("Exception in getting TRANSACTIONS");
            System.out.println(e.getMessage());
            return null;
        } finally {
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                // connection close failed.
                System.err.println(e.getMessage());
                return null;
            }
        }
        if (!first) {
            System.out.println("NOT FINDING transaction " + s);

            return null;
        }
        return trans.build();
    }

    ;

    @Override
    public DistributedTransaction save(DistributedTransaction s) {
        Connection connection = null;
        try {
            String updateQuery = "Insert OR REPLACE Into TRANSACTIONS (ID, TS)  VALUES(?, ?);";
            // create a database connection
            connection = DriverManager.getConnection(this.dbDataFolder + File.separator + this.databaseName + DB_SUFFIX);
            PreparedStatement statement = connection.prepareStatement(updateQuery);

            statement.setString(1, s.getId());
            System.out.println(s.getId());
            String jsonString = JsonFormat.printer().includingDefaultValueFields().print(s.toBuilder().build());
            System.out.println(jsonString);

            statement.setString(2, jsonString);
            statement.setQueryTimeout(5);  // set timeout to 30 sec.
            int result = statement.executeUpdate();
            statement.clearParameters();

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        } finally {
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                System.err.println(e.getMessage());
                return null;
            }
        }
        return s;
    }

    ;

    @Override
    public List<DistributedTransaction> findByIdIn(List<String> ids) {
        List<DistributedTransaction> ts = new ArrayList<DistributedTransaction>();
        for (int i = 0; i < ids.size(); i++) {
            DistributedTransaction dt = findOne(ids.get(i));
            if (dt != null) {
                ts.add(dt);
            }
        }
        return ts;
    }

}