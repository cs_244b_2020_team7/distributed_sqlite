package edu.stanford.cs244b.twopc;

import static edu.stanford.cs244b.sofajraft.service.SqliteRaftServer.DB_FOLDER_NAME;

import edu.stanford.cs244b.sofajraft.jdbc.*;
import edu.stanford.cs244b.twopc.proto.*;
import edu.stanford.cs244b.twopc.proto.DistributedTransactionOuterClass.*;
import edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.*;
import edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.*;
import java.io.File;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ParticipantLogRecordRepository implements LogRecordRepository {
    private String name;
    public final static String DB_SUFFIX = ".db";

    private final static ResultSetJsonExtractor resultSetJsonExtractor = new ResultSetJsonExtractor();
    private static final String CREATE_TABLE =
            "CREATE TABLE IF NOT EXISTS LOGRECORDS" +
                    " (ID TEXT PRIMARY KEY NOT NULL," +
                    " STATUS INTEGER NOT NULL," +
                    " CREATE_TIME INTEGER," +
                    " LAST_MODIFY_TIME INTEGER);";
    private String serverFilePath;
    private String dbDataFolder;

    public ParticipantLogRecordRepository(String serverFilePath) {
        this.name = "LOGRECORD";
        System.out.println("Log record path file is " + serverFilePath);
        SqliteJDBC sqliteJDBC = new SqliteJDBCImpl(serverFilePath);
        this.dbDataFolder = String.format("jdbc:sqlite:%s", serverFilePath + File.separator + DB_FOLDER_NAME);
        int i = sqliteJDBC.executeUpdate(name,
                CREATE_TABLE);
        if (i == -1) {
            System.out.println(i);
            System.out.println("Can't initialize logRecord");
        }
    }


    int ConvertStatusToInt(TransactionStatus.Status status){
        switch (status){
            case Start2PC:
                return 1;
            case Abort:
                return 2;
            case Commit:
                return 3;
            case Yes:
                return 4;
            case No:
                return 5;
        }
        return -1;
    }

    TransactionStatus.Status ConvertIntToStatus(int i) {
        switch (i) {
            case 1:
                return TransactionStatus.Status.Start2PC;
            case 2:
                return TransactionStatus.Status.Abort;
            case 3:
                return TransactionStatus.Status.Commit;
            case 4:
                return TransactionStatus.Status.Yes;
            default:
                return TransactionStatus.Status.No;
        }
    }

    @Override
    public LogRecord findOne(String transactionId) {
        LogRecord.Builder logRecord = LogRecord.newBuilder();
        Connection connection = null;
        boolean first = false;
        try {
            connection = DriverManager.getConnection(this.dbDataFolder + File.separator + this.name + DB_SUFFIX);
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.
            ResultSet result = statement.executeQuery("Select * FROM LOGRECORDS Where ID = '" + transactionId +"';");
            while (result.next()) {
                if (!first) {
                    break;
                }
                logRecord.setId(result.getString("ID"));
                TransactionStatus tr = TransactionStatus.newBuilder().setStatus(ConvertIntToStatus(result.getInt("STATUS"))).build();
                logRecord.setStatus(tr);
                logRecord.setCreateTime(result.getInt("CREATE_TIME"));
                logRecord.setLastModifyTime(result.getInt("LAST_MODIFY_TIME"));
                first = true;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        } finally {
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                // connection close failed.
                System.err.println(e.getMessage());
                return null;
            }
        }
        if (!first){
            return null;
        }
        return logRecord.build();
    }

    public LogRecord save(LogRecord s){
        Connection connection = null;

        try {
            String updateQuery = "INSERT OR REPLACE INTO LOGRECORDS (ID, STATUS, CREATE_TIME, LAST_MODIFY_TIME)" +
                    "  VALUES(?, ?, ?,?);";
            // create a database connection
            connection = DriverManager.getConnection(this.dbDataFolder + File.separator + this.name + DB_SUFFIX);
            PreparedStatement statement= connection.prepareStatement(updateQuery );
            statement.setString(1,s.getId());
            statement.setInt(2, ConvertStatusToInt(s.getStatus().getStatus()));
            statement.setLong(3, s.getCreateTime());
            statement.setLong(3, s.getLastModifyTime());
            statement.setQueryTimeout(30);  // set timeout to 30 sec.
            int result = statement.executeUpdate();
            statement.clearParameters();

        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        } finally {
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                // connection close failed.
                System.err.println(e.getMessage());
                return null;
            }
        }
        return s;
    };

    @Override
    public List<LogRecord> findByStatusInAndCreateTimeLessThan(Long timestamp){
        List<LogRecord> records = new ArrayList<LogRecord>();
        Connection connection = null;
        LogRecord.Builder logRecord;
        try {
            connection = DriverManager.getConnection(this.dbDataFolder + File.separator + this.name + DB_SUFFIX);
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.
            ResultSet result = statement.executeQuery("SELECT * FROM LOGRECORDS WHERE CREATE_TIME < " + timestamp +" AND STATUS = 1;");
            while (result.next()) {
                logRecord = LogRecord.newBuilder();
                logRecord.setId(result.getString("ID"));
                TransactionStatus tr = TransactionStatus.newBuilder().setStatus(ConvertIntToStatus(result.getInt("STATUS"))).build();
                logRecord.setStatus(tr);
                logRecord.setCreateTime(result.getInt("CREATE_TIME"));
                logRecord.setLastModifyTime(result.getInt("LAST_MODIFY_TIME"));
                records.add(logRecord.build());
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        } finally {
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                // connection close failed.
                System.err.println(e.getMessage());
                return null;
            }
        }
        return records;
    };

}