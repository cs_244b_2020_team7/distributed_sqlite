// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: LogRecord.proto

package edu.stanford.cs244b.twopc.proto;

public final class LogRecordOuterClass {
  private LogRecordOuterClass() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  public interface LogRecordOrBuilder extends
      // @@protoc_insertion_point(interface_extends:LogRecord)
      com.google.protobuf.MessageOrBuilder {

    /**
     * <code>string id = 1;</code>
     * @return The id.
     */
    java.lang.String getId();
    /**
     * <code>string id = 1;</code>
     * @return The bytes for id.
     */
    com.google.protobuf.ByteString
        getIdBytes();

    /**
     * <code>.TransactionStatus status = 2;</code>
     * @return Whether the status field is set.
     */
    boolean hasStatus();
    /**
     * <code>.TransactionStatus status = 2;</code>
     * @return The status.
     */
    edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatus getStatus();
    /**
     * <code>.TransactionStatus status = 2;</code>
     */
    edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatusOrBuilder getStatusOrBuilder();

    /**
     * <code>int64 create_Time = 3;</code>
     * @return The createTime.
     */
    long getCreateTime();

    /**
     * <code>int64 last_modify_time = 4;</code>
     * @return The lastModifyTime.
     */
    long getLastModifyTime();
  }
  /**
   * Protobuf type {@code LogRecord}
   */
  public  static final class LogRecord extends
      com.google.protobuf.GeneratedMessageV3 implements
      // @@protoc_insertion_point(message_implements:LogRecord)
      LogRecordOrBuilder {
  private static final long serialVersionUID = 0L;
    // Use LogRecord.newBuilder() to construct.
    private LogRecord(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
      super(builder);
    }
    private LogRecord() {
      id_ = "";
    }

    @java.lang.Override
    @SuppressWarnings({"unused"})
    protected java.lang.Object newInstance(
        UnusedPrivateParameter unused) {
      return new LogRecord();
    }

    @java.lang.Override
    public final com.google.protobuf.UnknownFieldSet
    getUnknownFields() {
      return this.unknownFields;
    }
    private LogRecord(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      this();
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      com.google.protobuf.UnknownFieldSet.Builder unknownFields =
          com.google.protobuf.UnknownFieldSet.newBuilder();
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              java.lang.String s = input.readStringRequireUtf8();

              id_ = s;
              break;
            }
            case 18: {
              edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatus.Builder subBuilder = null;
              if (status_ != null) {
                subBuilder = status_.toBuilder();
              }
              status_ = input.readMessage(edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatus.parser(), extensionRegistry);
              if (subBuilder != null) {
                subBuilder.mergeFrom(status_);
                status_ = subBuilder.buildPartial();
              }

              break;
            }
            case 24: {

              createTime_ = input.readInt64();
              break;
            }
            case 32: {

              lastModifyTime_ = input.readInt64();
              break;
            }
            default: {
              if (!parseUnknownField(
                  input, unknownFields, extensionRegistry, tag)) {
                done = true;
              }
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e).setUnfinishedMessage(this);
      } finally {
        this.unknownFields = unknownFields.build();
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.internal_static_LogRecord_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.internal_static_LogRecord_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord.class, edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord.Builder.class);
    }

    public static final int ID_FIELD_NUMBER = 1;
    private volatile java.lang.Object id_;
    /**
     * <code>string id = 1;</code>
     * @return The id.
     */
    public java.lang.String getId() {
      java.lang.Object ref = id_;
      if (ref instanceof java.lang.String) {
        return (java.lang.String) ref;
      } else {
        com.google.protobuf.ByteString bs = 
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        id_ = s;
        return s;
      }
    }
    /**
     * <code>string id = 1;</code>
     * @return The bytes for id.
     */
    public com.google.protobuf.ByteString
        getIdBytes() {
      java.lang.Object ref = id_;
      if (ref instanceof java.lang.String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        id_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }

    public static final int STATUS_FIELD_NUMBER = 2;
    private edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatus status_;
    /**
     * <code>.TransactionStatus status = 2;</code>
     * @return Whether the status field is set.
     */
    public boolean hasStatus() {
      return status_ != null;
    }
    /**
     * <code>.TransactionStatus status = 2;</code>
     * @return The status.
     */
    public edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatus getStatus() {
      return status_ == null ? edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatus.getDefaultInstance() : status_;
    }
    /**
     * <code>.TransactionStatus status = 2;</code>
     */
    public edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatusOrBuilder getStatusOrBuilder() {
      return getStatus();
    }

    public static final int CREATE_TIME_FIELD_NUMBER = 3;
    private long createTime_;
    /**
     * <code>int64 create_Time = 3;</code>
     * @return The createTime.
     */
    public long getCreateTime() {
      return createTime_;
    }

    public static final int LAST_MODIFY_TIME_FIELD_NUMBER = 4;
    private long lastModifyTime_;
    /**
     * <code>int64 last_modify_time = 4;</code>
     * @return The lastModifyTime.
     */
    public long getLastModifyTime() {
      return lastModifyTime_;
    }

    private byte memoizedIsInitialized = -1;
    @java.lang.Override
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      memoizedIsInitialized = 1;
      return true;
    }

    @java.lang.Override
    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      if (!getIdBytes().isEmpty()) {
        com.google.protobuf.GeneratedMessageV3.writeString(output, 1, id_);
      }
      if (status_ != null) {
        output.writeMessage(2, getStatus());
      }
      if (createTime_ != 0L) {
        output.writeInt64(3, createTime_);
      }
      if (lastModifyTime_ != 0L) {
        output.writeInt64(4, lastModifyTime_);
      }
      unknownFields.writeTo(output);
    }

    @java.lang.Override
    public int getSerializedSize() {
      int size = memoizedSize;
      if (size != -1) return size;

      size = 0;
      if (!getIdBytes().isEmpty()) {
        size += com.google.protobuf.GeneratedMessageV3.computeStringSize(1, id_);
      }
      if (status_ != null) {
        size += com.google.protobuf.CodedOutputStream
          .computeMessageSize(2, getStatus());
      }
      if (createTime_ != 0L) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt64Size(3, createTime_);
      }
      if (lastModifyTime_ != 0L) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt64Size(4, lastModifyTime_);
      }
      size += unknownFields.getSerializedSize();
      memoizedSize = size;
      return size;
    }

    @java.lang.Override
    public boolean equals(final java.lang.Object obj) {
      if (obj == this) {
       return true;
      }
      if (!(obj instanceof edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord)) {
        return super.equals(obj);
      }
      edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord other = (edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord) obj;

      if (!getId()
          .equals(other.getId())) return false;
      if (hasStatus() != other.hasStatus()) return false;
      if (hasStatus()) {
        if (!getStatus()
            .equals(other.getStatus())) return false;
      }
      if (getCreateTime()
          != other.getCreateTime()) return false;
      if (getLastModifyTime()
          != other.getLastModifyTime()) return false;
      if (!unknownFields.equals(other.unknownFields)) return false;
      return true;
    }

    @java.lang.Override
    public int hashCode() {
      if (memoizedHashCode != 0) {
        return memoizedHashCode;
      }
      int hash = 41;
      hash = (19 * hash) + getDescriptor().hashCode();
      hash = (37 * hash) + ID_FIELD_NUMBER;
      hash = (53 * hash) + getId().hashCode();
      if (hasStatus()) {
        hash = (37 * hash) + STATUS_FIELD_NUMBER;
        hash = (53 * hash) + getStatus().hashCode();
      }
      hash = (37 * hash) + CREATE_TIME_FIELD_NUMBER;
      hash = (53 * hash) + com.google.protobuf.Internal.hashLong(
          getCreateTime());
      hash = (37 * hash) + LAST_MODIFY_TIME_FIELD_NUMBER;
      hash = (53 * hash) + com.google.protobuf.Internal.hashLong(
          getLastModifyTime());
      hash = (29 * hash) + unknownFields.hashCode();
      memoizedHashCode = hash;
      return hash;
    }

    public static edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord parseFrom(
        java.nio.ByteBuffer data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord parseFrom(
        java.nio.ByteBuffer data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }
    public static edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input);
    }
    public static edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
    }
    public static edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }

    @java.lang.Override
    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder() {
      return DEFAULT_INSTANCE.toBuilder();
    }
    public static Builder newBuilder(edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord prototype) {
      return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }
    @java.lang.Override
    public Builder toBuilder() {
      return this == DEFAULT_INSTANCE
          ? new Builder() : new Builder().mergeFrom(this);
    }

    @java.lang.Override
    protected Builder newBuilderForType(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * Protobuf type {@code LogRecord}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
        // @@protoc_insertion_point(builder_implements:LogRecord)
        edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecordOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.internal_static_LogRecord_descriptor;
      }

      @java.lang.Override
      protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
          internalGetFieldAccessorTable() {
        return edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.internal_static_LogRecord_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord.class, edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord.Builder.class);
      }

      // Construct using edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessageV3
                .alwaysUseFieldBuilders) {
        }
      }
      @java.lang.Override
      public Builder clear() {
        super.clear();
        id_ = "";

        if (statusBuilder_ == null) {
          status_ = null;
        } else {
          status_ = null;
          statusBuilder_ = null;
        }
        createTime_ = 0L;

        lastModifyTime_ = 0L;

        return this;
      }

      @java.lang.Override
      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.internal_static_LogRecord_descriptor;
      }

      @java.lang.Override
      public edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord getDefaultInstanceForType() {
        return edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord.getDefaultInstance();
      }

      @java.lang.Override
      public edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord build() {
        edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      @java.lang.Override
      public edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord buildPartial() {
        edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord result = new edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord(this);
        result.id_ = id_;
        if (statusBuilder_ == null) {
          result.status_ = status_;
        } else {
          result.status_ = statusBuilder_.build();
        }
        result.createTime_ = createTime_;
        result.lastModifyTime_ = lastModifyTime_;
        onBuilt();
        return result;
      }

      @java.lang.Override
      public Builder clone() {
        return super.clone();
      }
      @java.lang.Override
      public Builder setField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          java.lang.Object value) {
        return super.setField(field, value);
      }
      @java.lang.Override
      public Builder clearField(
          com.google.protobuf.Descriptors.FieldDescriptor field) {
        return super.clearField(field);
      }
      @java.lang.Override
      public Builder clearOneof(
          com.google.protobuf.Descriptors.OneofDescriptor oneof) {
        return super.clearOneof(oneof);
      }
      @java.lang.Override
      public Builder setRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          int index, java.lang.Object value) {
        return super.setRepeatedField(field, index, value);
      }
      @java.lang.Override
      public Builder addRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          java.lang.Object value) {
        return super.addRepeatedField(field, value);
      }
      @java.lang.Override
      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord) {
          return mergeFrom((edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord other) {
        if (other == edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord.getDefaultInstance()) return this;
        if (!other.getId().isEmpty()) {
          id_ = other.id_;
          onChanged();
        }
        if (other.hasStatus()) {
          mergeStatus(other.getStatus());
        }
        if (other.getCreateTime() != 0L) {
          setCreateTime(other.getCreateTime());
        }
        if (other.getLastModifyTime() != 0L) {
          setLastModifyTime(other.getLastModifyTime());
        }
        this.mergeUnknownFields(other.unknownFields);
        onChanged();
        return this;
      }

      @java.lang.Override
      public final boolean isInitialized() {
        return true;
      }

      @java.lang.Override
      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord) e.getUnfinishedMessage();
          throw e.unwrapIOException();
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }

      private java.lang.Object id_ = "";
      /**
       * <code>string id = 1;</code>
       * @return The id.
       */
      public java.lang.String getId() {
        java.lang.Object ref = id_;
        if (!(ref instanceof java.lang.String)) {
          com.google.protobuf.ByteString bs =
              (com.google.protobuf.ByteString) ref;
          java.lang.String s = bs.toStringUtf8();
          id_ = s;
          return s;
        } else {
          return (java.lang.String) ref;
        }
      }
      /**
       * <code>string id = 1;</code>
       * @return The bytes for id.
       */
      public com.google.protobuf.ByteString
          getIdBytes() {
        java.lang.Object ref = id_;
        if (ref instanceof String) {
          com.google.protobuf.ByteString b = 
              com.google.protobuf.ByteString.copyFromUtf8(
                  (java.lang.String) ref);
          id_ = b;
          return b;
        } else {
          return (com.google.protobuf.ByteString) ref;
        }
      }
      /**
       * <code>string id = 1;</code>
       * @param value The id to set.
       * @return This builder for chaining.
       */
      public Builder setId(
          java.lang.String value) {
        if (value == null) {
    throw new NullPointerException();
  }
  
        id_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>string id = 1;</code>
       * @return This builder for chaining.
       */
      public Builder clearId() {
        
        id_ = getDefaultInstance().getId();
        onChanged();
        return this;
      }
      /**
       * <code>string id = 1;</code>
       * @param value The bytes for id to set.
       * @return This builder for chaining.
       */
      public Builder setIdBytes(
          com.google.protobuf.ByteString value) {
        if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
        
        id_ = value;
        onChanged();
        return this;
      }

      private edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatus status_;
      private com.google.protobuf.SingleFieldBuilderV3<
          edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatus, edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatus.Builder, edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatusOrBuilder> statusBuilder_;
      /**
       * <code>.TransactionStatus status = 2;</code>
       * @return Whether the status field is set.
       */
      public boolean hasStatus() {
        return statusBuilder_ != null || status_ != null;
      }
      /**
       * <code>.TransactionStatus status = 2;</code>
       * @return The status.
       */
      public edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatus getStatus() {
        if (statusBuilder_ == null) {
          return status_ == null ? edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatus.getDefaultInstance() : status_;
        } else {
          return statusBuilder_.getMessage();
        }
      }
      /**
       * <code>.TransactionStatus status = 2;</code>
       */
      public Builder setStatus(edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatus value) {
        if (statusBuilder_ == null) {
          if (value == null) {
            throw new NullPointerException();
          }
          status_ = value;
          onChanged();
        } else {
          statusBuilder_.setMessage(value);
        }

        return this;
      }
      /**
       * <code>.TransactionStatus status = 2;</code>
       */
      public Builder setStatus(
          edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatus.Builder builderForValue) {
        if (statusBuilder_ == null) {
          status_ = builderForValue.build();
          onChanged();
        } else {
          statusBuilder_.setMessage(builderForValue.build());
        }

        return this;
      }
      /**
       * <code>.TransactionStatus status = 2;</code>
       */
      public Builder mergeStatus(edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatus value) {
        if (statusBuilder_ == null) {
          if (status_ != null) {
            status_ =
              edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatus.newBuilder(status_).mergeFrom(value).buildPartial();
          } else {
            status_ = value;
          }
          onChanged();
        } else {
          statusBuilder_.mergeFrom(value);
        }

        return this;
      }
      /**
       * <code>.TransactionStatus status = 2;</code>
       */
      public Builder clearStatus() {
        if (statusBuilder_ == null) {
          status_ = null;
          onChanged();
        } else {
          status_ = null;
          statusBuilder_ = null;
        }

        return this;
      }
      /**
       * <code>.TransactionStatus status = 2;</code>
       */
      public edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatus.Builder getStatusBuilder() {
        
        onChanged();
        return getStatusFieldBuilder().getBuilder();
      }
      /**
       * <code>.TransactionStatus status = 2;</code>
       */
      public edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatusOrBuilder getStatusOrBuilder() {
        if (statusBuilder_ != null) {
          return statusBuilder_.getMessageOrBuilder();
        } else {
          return status_ == null ?
              edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatus.getDefaultInstance() : status_;
        }
      }
      /**
       * <code>.TransactionStatus status = 2;</code>
       */
      private com.google.protobuf.SingleFieldBuilderV3<
          edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatus, edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatus.Builder, edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatusOrBuilder> 
          getStatusFieldBuilder() {
        if (statusBuilder_ == null) {
          statusBuilder_ = new com.google.protobuf.SingleFieldBuilderV3<
              edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatus, edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatus.Builder, edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.TransactionStatusOrBuilder>(
                  getStatus(),
                  getParentForChildren(),
                  isClean());
          status_ = null;
        }
        return statusBuilder_;
      }

      private long createTime_ ;
      /**
       * <code>int64 create_Time = 3;</code>
       * @return The createTime.
       */
      public long getCreateTime() {
        return createTime_;
      }
      /**
       * <code>int64 create_Time = 3;</code>
       * @param value The createTime to set.
       * @return This builder for chaining.
       */
      public Builder setCreateTime(long value) {
        
        createTime_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>int64 create_Time = 3;</code>
       * @return This builder for chaining.
       */
      public Builder clearCreateTime() {
        
        createTime_ = 0L;
        onChanged();
        return this;
      }

      private long lastModifyTime_ ;
      /**
       * <code>int64 last_modify_time = 4;</code>
       * @return The lastModifyTime.
       */
      public long getLastModifyTime() {
        return lastModifyTime_;
      }
      /**
       * <code>int64 last_modify_time = 4;</code>
       * @param value The lastModifyTime to set.
       * @return This builder for chaining.
       */
      public Builder setLastModifyTime(long value) {
        
        lastModifyTime_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>int64 last_modify_time = 4;</code>
       * @return This builder for chaining.
       */
      public Builder clearLastModifyTime() {
        
        lastModifyTime_ = 0L;
        onChanged();
        return this;
      }
      @java.lang.Override
      public final Builder setUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return super.setUnknownFields(unknownFields);
      }

      @java.lang.Override
      public final Builder mergeUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return super.mergeUnknownFields(unknownFields);
      }


      // @@protoc_insertion_point(builder_scope:LogRecord)
    }

    // @@protoc_insertion_point(class_scope:LogRecord)
    private static final edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord DEFAULT_INSTANCE;
    static {
      DEFAULT_INSTANCE = new edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord();
    }

    public static edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord getDefaultInstance() {
      return DEFAULT_INSTANCE;
    }

    private static final com.google.protobuf.Parser<LogRecord>
        PARSER = new com.google.protobuf.AbstractParser<LogRecord>() {
      @java.lang.Override
      public LogRecord parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
        return new LogRecord(input, extensionRegistry);
      }
    };

    public static com.google.protobuf.Parser<LogRecord> parser() {
      return PARSER;
    }

    @java.lang.Override
    public com.google.protobuf.Parser<LogRecord> getParserForType() {
      return PARSER;
    }

    @java.lang.Override
    public edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.LogRecord getDefaultInstanceForType() {
      return DEFAULT_INSTANCE;
    }

  }

  private static final com.google.protobuf.Descriptors.Descriptor
    internal_static_LogRecord_descriptor;
  private static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_LogRecord_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\017LogRecord.proto\032\027TransactionStatus.pro" +
      "to\"j\n\tLogRecord\022\n\n\002id\030\001 \001(\t\022\"\n\006status\030\002 " +
      "\001(\0132\022.TransactionStatus\022\023\n\013create_Time\030\003" +
      " \001(\003\022\030\n\020last_modify_time\030\004 \001(\003B!\n\037edu.st" +
      "anford.cs244b.twopc.protob\006proto3"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
          edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.getDescriptor(),
        });
    internal_static_LogRecord_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_LogRecord_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_LogRecord_descriptor,
        new java.lang.String[] { "Id", "Status", "CreateTime", "LastModifyTime", });
    edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.getDescriptor();
  }

  // @@protoc_insertion_point(outer_class_scope)
}
