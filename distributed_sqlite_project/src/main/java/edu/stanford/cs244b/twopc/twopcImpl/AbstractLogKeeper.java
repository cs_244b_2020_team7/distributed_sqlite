package edu.stanford.cs244b.twopc;

import edu.stanford.cs244b.twopc.proto.*;
import edu.stanford.cs244b.twopc.proto.LogRecordOuterClass.*;
import edu.stanford.cs244b.twopc.proto.DistributedTransactionOuterClass.*;
import edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.*;

import java.util.List;

/*disclamer this code is modified based on github open sourced 2pc implementation */
public abstract class AbstractLogKeeper implements LogKeeper {

    @Override
    public void recordMsg(DistributedTransaction transaction, TransactionStatus type) {
        saveOrUpdate(createLogRecord(transaction, type));
    }

    private LogRecord createLogRecord(DistributedTransaction transaction, TransactionStatus status) {
        LogRecord log_record = LogRecord.newBuilder()
                .setId(transaction.getId())
                .setStatus(status)
                .build();
        return log_record;
    }

    public abstract void saveOrUpdate(LogRecord logRecord);

    public abstract void saveOrUpdate(DistributedTransaction distributedTransaction);

    public abstract List<LogRecord> findUndecidedTransactions();

    public abstract DistributedTransaction findTranactionById(String Id);

    public abstract LogRecord findLogRecordById(String Id);

    public abstract List<DistributedTransaction> findTranactionByIds(List<String> Id);
}