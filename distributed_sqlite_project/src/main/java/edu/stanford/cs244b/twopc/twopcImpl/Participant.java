package edu.stanford.cs244b.twopc;

import edu.stanford.cs244b.twopc.proto.*;
import edu.stanford.cs244b.twopc.proto.DistributedTransactionOuterClass.*;
import edu.stanford.cs244b.twopc.proto.DecisionMessage.*;
import edu.stanford.cs244b.twopc.proto.VoteResultOuterClass.*;
import edu.stanford.cs244b.twopc.proto.VoteOuterClass.*;
import com.alipay.sofa.jraft.rpc.impl.cli.CliClientServiceImpl;

/*disclamer this code is modified based on github open sourced 2pc implementation */
public interface Participant extends Node {
    // Need to have a vote handler
   // void vote(DistributedTransaction transaction, DistributedSqliteClosure closure);
    // Commit the transaction in local db.
    // commit handler
    void commit(String transaction_id);
    // abort handler
    // abort the transaction in local db.
    void abort(String transaction_id);
    // If goes down recover from this
    void recover();

    Decision resolveDecisionByCTP(CliClientServiceImpl client, DistributedTransaction transaction);
}