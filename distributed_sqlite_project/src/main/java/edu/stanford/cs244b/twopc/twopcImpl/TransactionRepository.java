package edu.stanford.cs244b.twopc;

import edu.stanford.cs244b.twopc.proto.DistributedTransactionOuterClass.*;

import java.util.List;

public interface TransactionRepository {

    DistributedTransaction findOne(String s);

    DistributedTransaction save(DistributedTransaction s);

    List<DistributedTransaction> findByIdIn(List<String> ids);
}