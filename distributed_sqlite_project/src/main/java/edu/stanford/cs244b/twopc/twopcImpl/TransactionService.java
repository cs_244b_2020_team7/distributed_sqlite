package edu.stanford.cs244b.twopc;

import edu.stanford.cs244b.twopc.proto.DistributedTransactionOuterClass.*;
import java.util.List;


public class TransactionService {
    private TransactionRepository transactionRepository;
    public TransactionService(String filePath){
        transactionRepository = new ParticipantTransactionRepo(filePath);
    }

    public void saveOrUpdate(DistributedTransaction distributedTransaction) {
        DistributedTransaction saving = transactionRepository.save(distributedTransaction);
    }

    public DistributedTransaction findTranactionById(String Id) {
        final DistributedTransaction transactionEntity = transactionRepository.findOne(Id);
        if (transactionEntity == null)
            return null;
        return transactionEntity;
    }

    public List<DistributedTransaction> findTranactionByIds(List<String> Id) {
        final List<DistributedTransaction> entities = transactionRepository.findByIdIn(Id);
        return entities;
    }
}