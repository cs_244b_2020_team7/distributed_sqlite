package edu.stanford.cs244b.twopc;

import com.alipay.sofa.jraft.entity.PeerId;
import com.alipay.sofa.jraft.rpc.InvokeCallback;
import com.alipay.sofa.jraft.rpc.impl.cli.*;
import com.alipay.sofa.jraft.rpc.impl.cli.CliClientServiceImpl;
import edu.stanford.cs244b.twopc.proto.*;
import edu.stanford.cs244b.twopc.proto.DecisionMessage.*;
import edu.stanford.cs244b.twopc.proto.DecisionRequest.*;
import edu.stanford.cs244b.twopc.proto.DistributedTransactionOuterClass.*;
import edu.stanford.cs244b.twopc.proto.DistributedTransactionVoteRequestOuterClass.*;
import edu.stanford.cs244b.twopc.proto.VoteOuterClass.*;
import edu.stanford.cs244b.twopc.proto.VoteResultOuterClass.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;
import edu.stanford.cs244b.rest.RpcClientFactory;

import com.alipay.sofa.jraft.RouteTable;

/*disclamer this code is modified based on github open sourced 2pc implementation */
public class MessageSenderImpl implements MessageSender {


    private Logger logger = LoggerFactory.getLogger(getClass());
    private int waitForVoteTimeoutSecs;

    public MessageSenderImpl() {
        this(10);
    }

    public MessageSenderImpl(int waitForVoteTimeoutSecs) {
        this.waitForVoteTimeoutSecs = 5000;
    }

    public int getWaitForVoteTimeoutSecs() {
        return 20;
    }

    public void setWaitForVoteTimeoutSecs(int waitForVoteTimeoutSecs) {
        if (waitForVoteTimeoutSecs > 0) {
            this.waitForVoteTimeoutSecs = waitForVoteTimeoutSecs;
        }
    }

    @Override
    public void sendVoteRequestToParticipant(CliClientServiceImpl cliClientService, String group,
                                             DistributedTransaction request,
                                             DistributedTransaction.Builder transacation_builder) {
        final CountDownLatch countDownLatch = new CountDownLatch(1);
        try {
            System.out.println("Corss DB vote request started. " + group);
            RouteTable.getInstance().refreshLeader(cliClientService, group, 1000).isOk();
            Vote.Builder v = Vote.newBuilder().setStatus(Vote.Status.No);
            final PeerId serverId = RouteTable.getInstance().selectLeader(group);
            VoteResult.Builder vote = VoteResult.newBuilder().setVoteResult(v);
            cliClientService.getRpcClient().invokeAsync(serverId.getEndpoint(), request, new InvokeCallback() {
                @Override
                public void complete(Object o, Throwable throwable) {
                    try {
                        VoteResult response = (VoteResult) o;
                        System.out.println("Cast response: " + response);
                        vote.mergeFrom(response);
                        transacation_builder.addVoteResult(response);
                        countDownLatch.countDown();
                    } catch (Exception e) {
                        System.out.println(e);
                        countDownLatch.countDown();
                    }
                }

                @Override
                public Executor executor() {
                    return null;
                }
            }, waitForVoteTimeoutSecs);
            countDownLatch.await();
        } catch (Exception e) {
            logger.error("Exception in querying message: ",group, request.getId(), e);
            Vote vote = Vote.newBuilder().setStatus(Vote.Status.Unknown).build();
            VoteResult voteResult = VoteResult.newBuilder().setTransactionId(request.getId())
                    .setVoteResult(vote)
                    .build();
            transacation_builder.addVoteResult(voteResult);
            countDownLatch.countDown();
        }
    }

    @Override
    public void sendDecisionToParticipant(CliClientServiceImpl cliClientService,
                                          String group, Decision message) {
        try {
            System.out.println("Corss DB vote request started. " + group);
            RouteTable.getInstance().refreshLeader(cliClientService, group, 1000).isOk();
            final PeerId serverId = RouteTable.getInstance().selectLeader(group);
            cliClientService.getRpcClient().invokeAsync(serverId.getEndpoint(), message, new InvokeCallback() {
                @Override
                public void complete(Object o, Throwable throwable) {
                }

                @Override
                public Executor executor() {
                    return null;
                }
            }, 5000);
            System.out.println("DB deicion send finished.");
        } catch (Exception ex) {
            logger.error("Exception in sending decision to participant {} for transaction {} ", group, message.getTransId(), ex);
        }
        return;
    }

    @Override
    public  void sendDecisionQuery2Others(CliClientServiceImpl cliClientService,
                                             Node node, DecisionRequest.DecisionMessage message,
                                             Decision.Builder decision) {
        final CountDownLatch countDownLatch = new CountDownLatch(1);
//        Decision decision = new Decision.newBuilder()
//                                        .setDecision(Decision.DecisionType.Abort)
//                                        .build();
        try {
            System.out.println("Send decision query to participants");
            final PeerId serverId = new PeerId();
            if (!serverId.parse(node.communicationAddr().toString())) {
                throw new IllegalArgumentException("Fail to parse serverId:" + node.communicationAddr().toString());
            }
            cliClientService.getRpcClient().invokeAsync(serverId.getEndpoint(), message, new InvokeCallback() {
                @Override
                public void complete(Object o, Throwable throwable) {
                    Decision response = (Decision) o;
                    System.out.println("Cast response: " + response);
                    decision.mergeFrom(response);
                    countDownLatch.countDown();
                }

                @Override
                public Executor executor() {
                    return null;
                }
            }, 5000);
            countDownLatch.await();
            System.out.println("DB decision send finished.");
        } catch (Exception ex) {

            logger.error("Decision query is having issue ", node.communicationAddr().toString(), message.getTransId(), ex);
            decision.mergeFrom(Decision.newBuilder().setDecision(Decision.DecisionType.Unknown)
                    .build());
            countDownLatch.countDown();
        }
    }

}