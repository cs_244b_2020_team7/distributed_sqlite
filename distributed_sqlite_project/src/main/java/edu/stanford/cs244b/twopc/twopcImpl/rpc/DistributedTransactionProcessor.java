package edu.stanford.cs244b.twopc.rpc;

import com.alipay.sofa.jraft.Status;
import com.alipay.sofa.jraft.rpc.RpcContext;
import com.alipay.sofa.jraft.rpc.RpcProcessor;
import edu.stanford.cs244b.twopc.proto.DistributedTransactionOuterClass.*;
import edu.stanford.cs244b.twopc.AbstractLogKeeper;
import edu.stanford.cs244b.twopc.proto.TransactionStatusOuterClass.*;
import edu.stanford.cs244b.sofajraft.service.DistributedSqliteClosure;
import edu.stanford.cs244b.twopc.DefaultCoordinator;
import edu.stanford.cs244b.twopc.proto.DecisionMessage.*;
import java.util.concurrent.CountDownLatch;

/**
 * Processor for read request to distributed sqlite.
 */
public class DistributedTransactionProcessor implements RpcProcessor<DistributedTransaction> {

    private DefaultCoordinator coordinator;
    public DistributedTransactionProcessor(DefaultCoordinator coordinator) {
        super();
        this.coordinator = coordinator;
    }

    @Override
    public void handleRequest(final RpcContext rpcCtx, final DistributedTransaction request) {
        CountDownLatch latch = new CountDownLatch(1);
        try {
            System.out.println("Receive request");
            Decision decision = this.coordinator.handleTransaction(request, latch);
            rpcCtx.sendResponse(decision);
        } catch(Exception ex){
            latch.countDown();
            System.out.println(ex.toString());
        }
    }

    @Override
    public String interest() {
        return DistributedTransaction.class.getName();
    }
}
