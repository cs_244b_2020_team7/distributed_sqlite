package edu.stanford.cs244b.twopc.util;

import java.util.UUID;

/*disclamer this code is modified based on github open sourced 2pc implementation */
public class TransactionIdGenerator {
    public static String getTransactionId() {
        return UUID.randomUUID().toString().replace("-", "");
    }

}