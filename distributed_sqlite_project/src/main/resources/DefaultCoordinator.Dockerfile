# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine

# copy the jar file
COPY ./target/distributed_sqlite_project-1.0-SNAPSHOT-jar-with-dependencies.jar /app.jar

# runs application
ENTRYPOINT ["/usr/lib/jvm/java-1.8-openjdk/jre/bin/java", "-cp", "/app.jar", "edu.stanford.cs244b.twopc.DefaultCoordinator"]
